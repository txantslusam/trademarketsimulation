package com.tradeMarketSimulation.App;

import com.tradeMarketSimulation.App.Controllers.MainWindow;
import com.tradeMarketSimulation.Core.Simulation;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;

public class Main extends Application {

    public static MainWindow MainWindowController;

    public static Simulation simulatorInstance;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Views/SplashScreen.fxml"));
        primaryStage.setTitle("Trading Market Simulator");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();

        simulatorInstance = new Simulation();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("Views/MainWindow.fxml"));
        Scene scene = new Scene(loader.load());
        File f = new File(getClass().getResource("style.css").toURI());
        String fileURI = f.toURI().toString();
        scene.getStylesheets().add(fileURI);
        primaryStage.setScene(scene);
        primaryStage.show();
        MainWindowController = loader.<MainWindow>getController();
    }
}
