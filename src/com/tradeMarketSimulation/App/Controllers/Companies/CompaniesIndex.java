package com.tradeMarketSimulation.App.Controllers.Companies;

import com.jfoenix.controls.JFXListCell;
import com.jfoenix.controls.JFXListView;
import com.tradeMarketSimulation.App.Main;
import com.tradeMarketSimulation.Core.company.Company;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import java.net.URL;
import java.util.ResourceBundle;

public class CompaniesIndex implements Initializable {
    @FXML
    public JFXListView menu;
    @FXML
    public VBox content;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<Company> companyObservableList = FXCollections.observableList(Main.simulatorInstance.companies);
        menu.setItems(companyObservableList);

        menu.setCellFactory(new Callback<JFXListView<Company>, JFXListCell<Company>>(){

            @Override
            public JFXListCell<Company> call(JFXListView<Company> p) {

                JFXListCell<Company> cell = new JFXListCell<>(){

                    @Override
                    protected void updateItem(Company company, boolean bln) {
                        super.updateItem(company, bln);
                    }

                };

                cell.setOnMouseClicked(e -> {
                    try {
                        selectItem();
                    } catch (Exception err) {

                    }
                });

                return cell;
            }

        });

    }
    public void selectItem() throws Exception {
        Company company = (Company) menu.getSelectionModel().getSelectedItem();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../../Views/Companies/CompaniesShow.fxml"));
        content.getChildren().setAll((HBox) loader.load());
        CompaniesShow controller = loader.<CompaniesShow>getController();
        controller.initData(company);
    }

    public void navigateToCreate(MouseEvent event) throws Exception {
        StackPane createLayout = FXMLLoader.load(getClass().getResource("../../Views/Companies/CompaniesCreate.fxml"));
        Main.MainWindowController.replaceTabContent("Company", createLayout);
    }
}
