package com.tradeMarketSimulation.App.Controllers.Companies;

import com.github.javafaker.Faker;
import com.jfoenix.controls.*;
import com.tradeMarketSimulation.App.Main;
import com.tradeMarketSimulation.Core.Exceptions.MissingRequiredFieldException;
import com.tradeMarketSimulation.Core.company.Company;
import com.tradeMarketSimulation.Core.tradeMarkets.Exchange;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

public class CompaniesCreate implements Initializable {
    @FXML
    StackPane container;
    @FXML
    JFXTextField name;
    @FXML
    Label nameValidation;
    @FXML
    JFXComboBox<Exchange> stockExchange;
    @FXML
    Label stockExchangeValidator;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        name.setText(Faker.instance().hipster().word() + " Ltd.");
        ObservableList<Exchange> exchangeObservableList = FXCollections.observableList(Main.simulatorInstance.exchanges);
        stockExchange.setItems(exchangeObservableList);
        stockExchange.getSelectionModel().select((int) Math.floor(Math.random() * exchangeObservableList.size()));
    }

    private void checkValidation() throws MissingRequiredFieldException {
        resetValidationFields();
        boolean isInvalid = false;
        if(name.getText().length() < 1) {
            nameValidation.setText("Name is required");
            isInvalid = true;
        }
        if(stockExchange.getSelectionModel().getSelectedIndex() == -1) {
            stockExchangeValidator.setText("Stock exchange is required");
            isInvalid = true;
        }
        if(isInvalid) {
            throw new MissingRequiredFieldException();
        }
    }

    private void resetValidationFields() {
        nameValidation.setText("");
        stockExchangeValidator.setText("");
    }

    private void spawnDialog() {
        JFXDialogLayout content = new JFXDialogLayout();
        content.setHeading(new Text("Successfully created Company"));
        JFXDialog dialog = new JFXDialog(container, content, JFXDialog.DialogTransition.CENTER);
        JFXButton button = new JFXButton("Okey");
        button.setOnAction(event -> {
            try {
                dialog.close();
                goBack();
            } catch (Exception e) {

            }
        });
        content.setActions(button);
        dialog.show();
    }

    public void saveCurrency() {
        try {
            checkValidation();
            Main.simulatorInstance.createNewCompany(new Company(
                    name.getText(),
                    Math.random() * 10,
                    stockExchange.getSelectionModel().getSelectedItem()
            ));
            spawnDialog();
        } catch (MissingRequiredFieldException e) {
            return;
        }
    }

    public void goBack() throws Exception {
        BorderPane CurrencyIndex = FXMLLoader.load(getClass().getResource("../../Views/Companies/CompaniesIndex.fxml"));
        Main.MainWindowController.replaceTabContent("Company", CurrencyIndex);
    }
}
