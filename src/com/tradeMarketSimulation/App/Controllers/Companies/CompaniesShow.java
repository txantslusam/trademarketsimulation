package com.tradeMarketSimulation.App.Controllers.Companies;

import com.jfoenix.controls.JFXTextField;
import com.tradeMarketSimulation.App.Main;
import com.tradeMarketSimulation.Core.company.Company;
import com.tradeMarketSimulation.Core.helpers.Utils;
import com.tradeMarketSimulation.Core.simulationUtlis.DateValueObject;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ConcurrentModificationException;
import java.util.ResourceBundle;

public class CompaniesShow implements Initializable {
    private Company company;
    @FXML
    Label name;
    @FXML
    Label actualValue;
    @FXML
    Label minimalValue;
    @FXML
    Label maximumValue;
    @FXML
    Label buyPrice;
    @FXML
    Label sellPrice;
    @FXML
    Label stockCount;
    @FXML
    Label profit;
    @FXML
    Label income;
    @FXML
    Label equityCapital;
    @FXML
    Label shareCapital;
    @FXML
    Label volume;
    @FXML
    Label sales;
    @FXML
    VBox lineChartContainer;
    @FXML
    JFXTextField buyoffPrice;
    @FXML
    Label buyoffPrompt;
    @FXML
    Label deletePrompt;

    private XYChart.Series<Number, Number> series = new XYChart.Series<>();

    public void initData(Company company) {
        this.company = company;
        name.textProperty().setValue(company.getName());
        actualValue.textProperty().setValue(Utils.formatDecimalToFourPlaces(company.getActualValue()));
        minimalValue.textProperty().setValue(Utils.formatDecimalToFourPlaces(company.getMinimalValue()));
        maximumValue.textProperty().setValue(Utils.formatDecimalToFourPlaces(company.getMaximumValue()));
        buyPrice.textProperty().setValue(Utils.formatDecimalToFourPlaces(company.getBuyPrice()));
        sellPrice.textProperty().setValue(Utils.formatDecimalToFourPlaces(company.getSellPrice()));
        profit.textProperty().setValue(Utils.formatDecimalToFourPlaces(company.getProfit()));
        income.textProperty().setValue(Utils.formatDecimalToFourPlaces(company.getIncome()));
        equityCapital.textProperty().setValue(Utils.formatDecimalToFourPlaces(company.getEquityCapital()));
        shareCapital.textProperty().setValue(Utils.formatDecimalToFourPlaces(company.getShareCapital()));
        sales.textProperty().setValue(Utils.formatDecimalToFourPlaces(company.getSales()));
        stockCount.textProperty().setValue(String.valueOf(company.getStockCount()));
        volume.textProperty().setValue(String.valueOf(company.getVolume()));

        initLineChart(company.getPriceChangeHistory());

        company.ActualValue().addListener((observable, oldValue, newValue) -> {
            actualValue.textProperty().setValue(Utils.formatDecimalToFourPlaces(newValue));
        });
        company.MinimalValue().addListener((observable, oldValue, newValue) -> {
            minimalValue.textProperty().setValue(Utils.formatDecimalToFourPlaces(newValue));
        });
        company.MaximumValue().addListener((observable, oldValue, newValue) -> {
            maximumValue.textProperty().setValue(Utils.formatDecimalToFourPlaces(newValue));
        });
        company.BuyPrice().addListener((observable, oldValue, newValue) -> {
            buyPrice.textProperty().setValue(Utils.formatDecimalToFourPlaces(newValue));
        });
        company.SellPrice().addListener((observable, oldValue, newValue) -> {
            sellPrice.textProperty().setValue(Utils.formatDecimalToFourPlaces(newValue));
        });
        company.profitProperty().addListener((observable, oldValue, newValue) -> {
            profit.textProperty().setValue(Utils.formatDecimalToFourPlaces(newValue));
        });
        company.incomeProperty().addListener((observable, oldValue, newValue) -> {
            income.textProperty().setValue(Utils.formatDecimalToFourPlaces(newValue));
        });
        company.equityCapitalProperty().addListener((observable, oldValue, newValue) -> {
            equityCapital.textProperty().setValue(Utils.formatDecimalToFourPlaces(newValue));
        });
        company.shareCapitalProperty().addListener((observable, oldValue, newValue) -> {
            shareCapital.textProperty().setValue(Utils.formatDecimalToFourPlaces(newValue));
        });
        company.stockCountProperty().addListener((observable, oldValue, newValue) -> {
            stockCount.textProperty().setValue(String.valueOf(newValue));
        });
        company.volumeProperty().addListener((observable, oldValue, newValue) -> {
            volume.textProperty().setValue(String.valueOf(newValue));
        });
        company.salesProperty().addListener((observable, oldValue, newValue) -> {
            sales.textProperty().setValue(Utils.formatDecimalToFourPlaces(newValue));
        });

    }

    private void initLineChart(ObservableList<DateValueObject<Double>> priceChangeHistory) {
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        // yAxis setup
        yAxis.setAutoRanging(false);
        if (priceChangeHistory.get(0).getValue() - 1 < 0) {
            yAxis.setLowerBound(0);
        } else {
            yAxis.setLowerBound(priceChangeHistory.get(0).getValue() - 1);
        }
        yAxis.setUpperBound(priceChangeHistory.get(0).getValue() + 1);
        yAxis.setTickUnit(0.1);
        yAxis.setLabel("Currency rate");
        series.setName("Currency rate");

        // xAxis setup
        xAxis.setLabel("Time");
        xAxis.setAutoRanging(false);
        xAxis.setTickLabelRotation(45);
        if (priceChangeHistory.size() > 30) {
            for (int i = priceChangeHistory.size() - 31; i < priceChangeHistory.size(); i++) {
                series.getData().add(new XYChart.Data<>(i, priceChangeHistory.get(i).getValue()));
            }
            xAxis.setLowerBound(priceChangeHistory.size() - 31);
            xAxis.setUpperBound(priceChangeHistory.size() - 1);
        } else {
            for (int i = 0; i < priceChangeHistory.size(); i++) {
                series.getData().add(new XYChart.Data<>(i, priceChangeHistory.get(i).getValue()));
            }
            xAxis.setLowerBound(0);
            xAxis.setUpperBound(priceChangeHistory.size()  - 1);
        }

        priceChangeHistory.addListener((ListChangeListener<DateValueObject<Double>>) change -> {
            if (change.next()) {
                for (int i = change.getFrom(); i < change.getTo(); i++) {
                    series.getData().add(new XYChart.Data<>(i, priceChangeHistory.get(i).getValue()));
                    xAxis.setUpperBound(i);
                    if (i > 30) {
                        xAxis.setLowerBound(i - 30);
                    }
                }
            }
        });
        xAxis.setTickLabelFormatter(new StringConverter<>() {
            @Override
            public String toString(Number object) {
                DateFormat dateFormat = new SimpleDateFormat("DD.MM.YYYY HH:mm");
                return dateFormat.format(priceChangeHistory.get(object.intValue()).getDate());
            }

            @Override
            public Number fromString(String string) {
                return null;
            }
        });
        LineChart<Number, Number> lineChart = new LineChart<>(xAxis, yAxis);
        lineChart.setAnimated(false);
        lineChart.getData().add(series);

        lineChartContainer.getChildren().add(lineChart);
    }

    public void buyoffStocks() {
        double price = Double.valueOf(buyoffPrice.getText());
        int stockBuyoffCount = company.buyoutStock(price);
        buyoffPrompt.setText("Stocks buyoff:" + stockBuyoffCount);
    }

    public void deleteCompany() throws IOException {
        try {
            Main.simulatorInstance.deleteCompany(this.company);
            BorderPane index = FXMLLoader.load(getClass().getResource("../../Views/Companies/CompaniesIndex.fxml"));
            Main.MainWindowController.replaceTabContent("Company", index);
        } catch (ConcurrentModificationException e) {
            deletePrompt.setText("Wooops, something went wrong.");
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
