package com.tradeMarketSimulation.App.Controllers.Currencies;

import com.github.javafaker.Faker;
import com.jfoenix.controls.*;
import com.tradeMarketSimulation.App.Main;
import com.tradeMarketSimulation.Core.Exceptions.MissingRequiredFieldException;
import com.tradeMarketSimulation.Core.tradeItems.Currency;
import com.tradeMarketSimulation.Core.tradeMarkets.CurrencyMarket;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

public class CurrenciesCreate implements Initializable {
    @FXML
    StackPane container;
    @FXML
    JFXTextField name;
    @FXML
    Label nameValidation;
    @FXML
    JFXComboBox<CurrencyMarket> currencyMarket;
    @FXML
    Label currencyMarketValidator;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        name.setText(Faker.instance().pokemon().name() + "Coin");
        ObservableList<CurrencyMarket> currencyMarkets = FXCollections.observableList(Main.simulatorInstance.currencyMarkets);
        currencyMarket.setItems(currencyMarkets);
        currencyMarket.getSelectionModel().select((int) Math.floor(Math.random() * currencyMarkets.size()));
    }

    private void checkValidation() throws MissingRequiredFieldException {
        resetValidationFields();
        boolean isInvalid = false;
        if(name.getText().length() < 1) {
            nameValidation.setText("Name is required");
            isInvalid = true;
        }
        if(currencyMarket.getSelectionModel().getSelectedIndex() == -1) {
            currencyMarketValidator.setText("Material market is required");
            isInvalid = true;
        }
        if(isInvalid) {
            throw new MissingRequiredFieldException();
        }
    }

    private void resetValidationFields() {
        nameValidation.setText("");
        currencyMarketValidator.setText("");
    }

    private void spawnDialog() {
        JFXDialogLayout content = new JFXDialogLayout();
        content.setHeading(new Text("Successfully created Currency"));
        JFXDialog dialog = new JFXDialog(container, content, JFXDialog.DialogTransition.CENTER);
        JFXButton button = new JFXButton("Okey");
        button.setOnAction(event -> {
            try {
                dialog.close();
                goBack();
            } catch (Exception e) {

            }
        });
        content.setActions(button);
        dialog.show();
    }

    public void saveCurrency() {
        try {
            checkValidation();
            Main.simulatorInstance.createNewCurrency(new Currency(
                name.getText(),
                Math.random() * 10,
                currencyMarket.getSelectionModel().getSelectedItem()
            ));
            spawnDialog();
        } catch (MissingRequiredFieldException e) {
            return;
        }
    }

    public void goBack() throws Exception {
        BorderPane CurrencyIndex = FXMLLoader.load(getClass().getResource("../../Views/Currencies/CurrenciesIndex.fxml"));
        Main.MainWindowController.replaceTabContent("Currency", CurrencyIndex);
    }

}
