package com.tradeMarketSimulation.App.Controllers.Currencies;

import com.jfoenix.controls.JFXListCell;
import com.jfoenix.controls.JFXListView;
import com.tradeMarketSimulation.App.Main;
import com.tradeMarketSimulation.Core.tradeItems.Currency;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import java.net.URL;
import java.util.ResourceBundle;

public class CurrenciesIndex implements Initializable{
    @FXML
    public JFXListView menu;
    @FXML
    public VBox content;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<Currency> currencyObservableList = FXCollections.observableList(Main.simulatorInstance.currencies);
        menu.setItems(currencyObservableList);

        menu.setCellFactory(new Callback<JFXListView<Currency>, JFXListCell<Currency>>(){

            @Override
            public JFXListCell<Currency> call(JFXListView<Currency> p) {

                JFXListCell<Currency> cell = new JFXListCell<>(){

                    @Override
                    protected void updateItem(Currency currency, boolean bln) {
                        super.updateItem(currency, bln);
                    }

                };

                cell.setOnMouseClicked(e -> {
                    try {
                        selectItem();
                    } catch (Exception err) {

                    }
                });

                return cell;
            }

        });

    }
    public void selectItem() throws Exception {
        Currency currency = (Currency) menu.getSelectionModel().getSelectedItem();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../../Views/Currencies/CurrenciesShow.fxml"));
        content.getChildren().setAll((HBox) loader.load());
        CurrenciesShow controller = loader.<CurrenciesShow>getController();
        controller.initData(currency);
    }

    public void navigateToCreate(MouseEvent event) throws Exception {
        StackPane createLayout = FXMLLoader.load(getClass().getResource("../../Views/Currencies/CurrenciesCreate.fxml"));
        Main.MainWindowController.replaceTabContent("Currency", createLayout);
    }
}
