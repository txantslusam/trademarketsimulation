package com.tradeMarketSimulation.App.Controllers.Indicies;

import com.tradeMarketSimulation.App.Main;
import com.tradeMarketSimulation.Core.company.Company;
import com.tradeMarketSimulation.Core.helpers.Utils;
import com.tradeMarketSimulation.Core.stockIndexes.StockIndex;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.LinkedHashSet;
import java.util.ResourceBundle;

public class IndiciesShow implements Initializable {
    private StockIndex index;
    @FXML
    Label name;
    @FXML
    Label actualValue;
    @FXML
    TableView companies;
    @FXML
    Label deletePrompt;

    public void initData(StockIndex index) {
        this.index = index;
        name.textProperty().setValue(index.getName());
        actualValue.textProperty().setValue(Utils.formatDecimalToFourPlaces(index.getValue()));
        initCompaniesTable(index.getCompanies());
    }

    private void initCompaniesTable(LinkedHashSet<Company> companies) {
        ObservableList<Company> countryObservableList = FXCollections.observableList(new ArrayList<>(companies));
        this.companies.setItems(countryObservableList);
        TableColumn<Company, String> col = new TableColumn<>("Company");
        this.companies.maxWidth(200);
        col.setPrefWidth(200);
        col.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getName()));
        this.companies.getColumns().setAll(col);
    }

    public void deleteIndex() throws IOException {
        try {
            Main.simulatorInstance.deleteStockIndex(this.index);
            BorderPane index = FXMLLoader.load(getClass().getResource("../../Views/Indicies/IndiciesIndex.fxml"));
            Main.MainWindowController.replaceTabContent("Index", index);
        } catch (ConcurrentModificationException e) {
            deletePrompt.setText("Wooops, something went wrong.");
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
