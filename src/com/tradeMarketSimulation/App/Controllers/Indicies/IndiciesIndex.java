package com.tradeMarketSimulation.App.Controllers.Indicies;

import com.jfoenix.controls.JFXListCell;
import com.jfoenix.controls.JFXListView;
import com.tradeMarketSimulation.App.Main;
import com.tradeMarketSimulation.Core.stockIndexes.StockIndex;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import java.net.URL;
import java.util.ResourceBundle;

public class IndiciesIndex implements Initializable {
    @FXML
    public JFXListView menu;
    @FXML
    public VBox content;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<StockIndex> currencyObservableList = FXCollections.observableList(Main.simulatorInstance.stockIndices);
        menu.setItems(currencyObservableList);

        menu.setCellFactory(new Callback<JFXListView<StockIndex>, JFXListCell<StockIndex>>(){

            @Override
            public JFXListCell<StockIndex> call(JFXListView<StockIndex> p) {

                JFXListCell<StockIndex> cell = new JFXListCell<>(){

                    @Override
                    protected void updateItem(StockIndex index, boolean bln) {
                        super.updateItem(index, bln);
                    }

                };

                cell.setOnMouseClicked(e -> {
                    try {
                        selectItem();
                    } catch (Exception err) {

                    }
                });

                return cell;
            }

        });

    }
    public void selectItem() throws Exception {
        StockIndex currency = (StockIndex) menu.getSelectionModel().getSelectedItem();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../../Views/Indicies/IndiciesShow.fxml"));
        content.getChildren().setAll((HBox) loader.load());
        IndiciesShow controller = loader.<IndiciesShow>getController();
        controller.initData(currency);
    }

    public void navigateToCreate(MouseEvent event) throws Exception {
        StackPane createLayout = FXMLLoader.load(getClass().getResource("../../Views/Indicies/IndiciesCreate.fxml"));
        Main.MainWindowController.replaceTabContent("Index", createLayout);
    }
}
