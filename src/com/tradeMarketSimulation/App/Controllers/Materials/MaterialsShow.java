package com.tradeMarketSimulation.App.Controllers.Materials;

import com.tradeMarketSimulation.App.Main;
import com.tradeMarketSimulation.Core.helpers.Utils;
import com.tradeMarketSimulation.Core.simulationUtlis.DateValueObject;
import com.tradeMarketSimulation.Core.tradeItems.Material;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ConcurrentModificationException;
import java.util.ResourceBundle;

public class MaterialsShow implements Initializable {
    private Material material;
    @FXML
    Label name;
    @FXML
    Label actualValue;
    @FXML
    Label minimalValue;
    @FXML
    Label maximumValue;
    @FXML
    Label buyPrice;
    @FXML
    Label sellPrice;
    @FXML
    Label Currency;
    @FXML
    Label materialUnit;
    @FXML
    VBox lineChartContainer;
    @FXML
    Label deletePrompt;

    private XYChart.Series<Number, Number> series = new XYChart.Series<>();

    public void initData(Material material) {
        this.material = material;
        name.textProperty().setValue(material.getName());
        actualValue.textProperty().setValue(Utils.formatDecimalToFourPlaces(material.getActualValue()));
        minimalValue.textProperty().setValue(Utils.formatDecimalToFourPlaces(material.getMinimalValue()));
        maximumValue.textProperty().setValue(Utils.formatDecimalToFourPlaces(material.getMaximumValue()));
        buyPrice.textProperty().setValue(Utils.formatDecimalToFourPlaces(material.getBuyPrice()));
        sellPrice.textProperty().setValue(Utils.formatDecimalToFourPlaces(material.getSellPrice()));
        Currency.textProperty().setValue(material.getCurrency().getName());
        materialUnit.textProperty().setValue(material.getMaterialUnit());
        initLineChart(material.getPriceChangeHistory());
        material.ActualValue().addListener((observable, oldValue, newValue) -> {
            actualValue.textProperty().setValue(Utils.formatDecimalToFourPlaces(newValue));
        });
        material.MinimalValue().addListener((observable, oldValue, newValue) -> {
            minimalValue.textProperty().setValue(Utils.formatDecimalToFourPlaces(newValue));
        });
        material.MaximumValue().addListener((observable, oldValue, newValue) -> {
            maximumValue.textProperty().setValue(Utils.formatDecimalToFourPlaces(newValue));
        });
        material.BuyPrice().addListener((observable, oldValue, newValue) -> {
            buyPrice.textProperty().setValue(Utils.formatDecimalToFourPlaces(newValue));
        });
        material.SellPrice().addListener((observable, oldValue, newValue) -> {
            sellPrice.textProperty().setValue(Utils.formatDecimalToFourPlaces(newValue));
        });
    }

    private void initLineChart(ObservableList<DateValueObject<Double>> priceChangeHistory) {
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        // yAxis setup
        yAxis.setAutoRanging(false);
        if (priceChangeHistory.get(0).getValue() - 1 < 0) {
            yAxis.setLowerBound(0);
        } else {
            yAxis.setLowerBound(priceChangeHistory.get(0).getValue() - 1);
        }
        yAxis.setUpperBound(priceChangeHistory.get(0).getValue() + 1);
        yAxis.setTickUnit(0.1);
        yAxis.setLabel("Material price");
        series.setName("Material price");

        // xAxis setup
        xAxis.setLabel("Time");
        xAxis.setAutoRanging(false);
        xAxis.setTickLabelRotation(45);
        if (priceChangeHistory.size() > 30) {
            for (int i = priceChangeHistory.size() - 31; i < priceChangeHistory.size(); i++) {
                series.getData().add(new XYChart.Data<>(i, priceChangeHistory.get(i).getValue()));
            }
            xAxis.setLowerBound(priceChangeHistory.size() - 31);
            xAxis.setUpperBound(priceChangeHistory.size() - 1);
        } else {
            for (int i = 0; i < priceChangeHistory.size(); i++) {
                series.getData().add(new XYChart.Data<>(i, priceChangeHistory.get(i).getValue()));
            }
            xAxis.setLowerBound(0);
            xAxis.setUpperBound(priceChangeHistory.size()  - 1);
        }

        priceChangeHistory.addListener((ListChangeListener<DateValueObject<Double>>) change -> {
            if (change.next()) {
                for (int i = change.getFrom(); i < change.getTo(); i++) {
                    series.getData().add(new XYChart.Data<>(i, priceChangeHistory.get(i).getValue()));
                    xAxis.setUpperBound(i);
                    if (i > 30) {
                        xAxis.setLowerBound(i - 30);
                    }
                }
            }
        });
        xAxis.setTickLabelFormatter(new StringConverter<>() {
            @Override
            public String toString(Number object) {
                DateFormat dateFormat = new SimpleDateFormat("DD.MM.YYYY HH:mm");
                return dateFormat.format(priceChangeHistory.get(object.intValue()).getDate());
            }

            @Override
            public Number fromString(String string) {
                return null;
            }
        });
        LineChart<Number, Number> lineChart = new LineChart<>(xAxis, yAxis);
        lineChart.setAnimated(false);
        lineChart.getData().add(series);

        lineChartContainer.getChildren().add(lineChart);
    }

    public void deleteMaterial() throws IOException {
        try {
            Main.simulatorInstance.deleteMaterial(this.material);
            BorderPane index = FXMLLoader.load(getClass().getResource("../../Views/Materials/MaterialsIndex.fxml"));
            Main.MainWindowController.replaceTabContent("Material", index);
        } catch (ConcurrentModificationException e) {
            deletePrompt.setText("Wooops, something went wrong.");
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
