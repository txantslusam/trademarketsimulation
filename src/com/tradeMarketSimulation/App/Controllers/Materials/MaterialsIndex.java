package com.tradeMarketSimulation.App.Controllers.Materials;

import com.jfoenix.controls.JFXListCell;
import com.jfoenix.controls.JFXListView;
import com.tradeMarketSimulation.App.Main;
import com.tradeMarketSimulation.Core.tradeItems.Material;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import java.net.URL;
import java.util.ResourceBundle;

public class MaterialsIndex implements Initializable {
    @FXML
    public JFXListView menu;
    @FXML
    public VBox content;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<Material> materialObservableList = FXCollections.observableList(Main.simulatorInstance.materials);
        menu.setItems(materialObservableList);

        menu.setCellFactory(new Callback<JFXListView<Material>, JFXListCell<Material>>(){
            @Override
            public JFXListCell<Material> call(JFXListView<Material> p) {

                JFXListCell<Material> cell = new JFXListCell<>(){
                    @Override
                    protected void updateItem(Material material, boolean bln) {
                        super.updateItem(material, bln);
                    }

                };
                cell.setOnMouseClicked(e -> {
                    try {
                        selectItem();
                    } catch (Exception err) {

                    }
                });
                return cell;
            }
        });

    }
    public void selectItem() throws Exception {
        Material currency = (Material) menu.getSelectionModel().getSelectedItem();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../../Views/Materials/MaterialsShow.fxml"));
        content.getChildren().setAll((HBox) loader.load());
        MaterialsShow controller = loader.<MaterialsShow>getController();
        controller.initData(currency);
    }

    public void navigateToCreate(MouseEvent event) throws Exception {
        StackPane createLayout = FXMLLoader.load(getClass().getResource("../../Views/Materials/MaterialsCreate.fxml"));
        Main.MainWindowController.replaceTabContent("Material", createLayout);
    }
}
