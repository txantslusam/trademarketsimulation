package com.tradeMarketSimulation.App.Controllers.Materials;

import com.github.javafaker.Faker;
import com.jfoenix.controls.*;
import com.tradeMarketSimulation.App.Main;
import com.tradeMarketSimulation.Core.Exceptions.MissingRequiredFieldException;
import com.tradeMarketSimulation.Core.simulationUtlis.MaterialUnit;
import com.tradeMarketSimulation.Core.tradeItems.Currency;
import com.tradeMarketSimulation.Core.tradeItems.Material;
import com.tradeMarketSimulation.Core.tradeMarkets.MaterialMarket;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

public class MaterialsCreate implements Initializable {
    @FXML
    StackPane container;
    @FXML
    JFXTextField name;
    @FXML
    Label nameValidation;
    @FXML
    JFXComboBox<String> materialUnit;
    @FXML
    Label materialUnitValidation;
    @FXML
    JFXComboBox<Currency> currency;
    @FXML
    Label currencyValidator;
    @FXML
    JFXComboBox<MaterialMarket> materialMarket;
    @FXML
    Label materialMarketValidator;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        name.setText(Faker.instance().beer().name());
        ObservableList<String> materialUnitsList = FXCollections.observableList(MaterialUnit.getMaterialUnits());
        materialUnit.setItems(materialUnitsList);
        materialUnit.getSelectionModel().select((int) Math.floor(Math.random() * materialUnitsList.size()));
        ObservableList<Currency> currencyList = FXCollections.observableList(Main.simulatorInstance.currencies);
        currency.setItems(currencyList);
        currency.getSelectionModel().select((int) Math.floor(Math.random() * currencyList.size()));
        ObservableList<MaterialMarket> materialMarkets = FXCollections.observableList(Main.simulatorInstance.materialMarkets);
        materialMarket.setItems(materialMarkets);
        materialMarket.getSelectionModel().select((int) Math.floor(Math.random() * materialMarkets.size()));
    }

    private void checkValidation() throws MissingRequiredFieldException {
        resetValidationFields();
        boolean isInvalid = false;
        if(name.getText().length() < 1) {
            nameValidation.setText("Name is required");
            isInvalid = true;
        }
        if(materialUnit.getSelectionModel().getSelectedIndex() == -1) {
            materialUnitValidation.setText("Material unit is required");
            isInvalid = true;
        }
        if(currency.getSelectionModel().getSelectedIndex() == -1) {
            currencyValidator.setText("Currency is required");
            isInvalid = true;
        }
        if(materialMarket.getSelectionModel().getSelectedIndex() == -1) {
            materialMarketValidator.setText("Material market is required");
            isInvalid = true;
        }
        if(isInvalid) {
            throw new MissingRequiredFieldException();
        }
    }

    private void resetValidationFields() {
        nameValidation.setText("");
        materialUnitValidation.setText("");
        currencyValidator.setText("");
        materialMarketValidator.setText("");
    }

    private void spawnDialog() {
        JFXDialogLayout content = new JFXDialogLayout();
        content.setHeading(new Text("Successfully created Material"));
        JFXDialog dialog = new JFXDialog(container, content, JFXDialog.DialogTransition.CENTER);
        JFXButton button = new JFXButton("Okey");
        button.setOnAction(event -> {
            try {
                dialog.close();
                goBack();
            } catch (Exception e) {

            }
        });
        content.setActions(button);
        dialog.show();
    }

    public void saveMaterial() {
        try {
            checkValidation();
            Main.simulatorInstance.createNewMaterial(new Material(
                name.getText(),
                Math.random() * 10,
                materialUnit.getSelectionModel().getSelectedItem(),
                currency.getSelectionModel().getSelectedItem(),
                materialMarket.getSelectionModel().getSelectedItem()
            ));
            spawnDialog();
        } catch (MissingRequiredFieldException e) {
            return;
        }
    }

    public void goBack() throws Exception {
        BorderPane MaterialsIndex = FXMLLoader.load(getClass().getResource("../../Views/Materials/MaterialsIndex.fxml"));
        Main.MainWindowController.replaceTabContent("Material", MaterialsIndex);
    }
}
