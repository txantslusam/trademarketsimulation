package com.tradeMarketSimulation.App.Controllers.CurrencyMarket;

import com.tradeMarketSimulation.App.Main;
import com.tradeMarketSimulation.Core.Exceptions.NoMoreTradeMarketException;
import com.tradeMarketSimulation.Core.helpers.Utils;
import com.tradeMarketSimulation.Core.tradeItems.Currency;
import com.tradeMarketSimulation.Core.tradeMarkets.CurrencyMarket;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.ResourceBundle;

public class CurrencyMarketShow implements Initializable {
    private CurrencyMarket currencyMarket;
    @FXML
    Label name;
    @FXML
    Label transactionMargin;
    @FXML
    TableView currencies;
    @FXML
    Label deletePrompt;

    public void initData(CurrencyMarket currencyMarket) {
        this.currencyMarket = currencyMarket;
        name.textProperty().setValue(currencyMarket.getName());
        transactionMargin.textProperty().setValue(Utils.formatDecimalToFourPlaces(currencyMarket.getTransactionMargin()));
        initMaterialsTable(currencyMarket.getCurrencies());
    }

    private void initMaterialsTable(ArrayList<Currency> materials) {
        ObservableList<Currency> currencyObservableList = FXCollections.observableList(materials);
        this.currencies.setItems(currencyObservableList);
        TableColumn<Currency, String> col = new TableColumn<>("Currency");
        this.currencies.maxWidth(200);
        col.setPrefWidth(200);
        col.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getName()));
        this.currencies.getColumns().setAll(col);
    }

    public void deleteCurrencyMarket() throws IOException {
        try {
            Main.simulatorInstance.deleteCurrencyMarket(this.currencyMarket);
            BorderPane index = FXMLLoader.load(getClass().getResource("../../Views/CurrencyMarket/CurrencyMarketIndex.fxml"));
            Main.MainWindowController.replaceTabContent("CurrencyMarket", index);
        } catch (NoMoreTradeMarketException e) {
            deletePrompt.setText("You can't remove last market");
        } catch (ConcurrentModificationException e) {
            deletePrompt.setText("Wooops, something went wrong.");
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
