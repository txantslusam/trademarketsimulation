package com.tradeMarketSimulation.App.Controllers.CurrencyMarket;

import com.jfoenix.controls.JFXListCell;
import com.jfoenix.controls.JFXListView;
import com.tradeMarketSimulation.App.Main;
import com.tradeMarketSimulation.Core.tradeMarkets.CurrencyMarket;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import java.net.URL;
import java.util.ResourceBundle;

public class CurrencyMarketIndex implements Initializable {
    @FXML
    public JFXListView menu;
    @FXML
    public VBox content;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<CurrencyMarket> currencyMarketObservableList = FXCollections.observableList(Main.simulatorInstance.currencyMarkets);
        menu.setItems(currencyMarketObservableList);

        menu.setCellFactory(new Callback<JFXListView<CurrencyMarket>, JFXListCell<CurrencyMarket>>(){

            @Override
            public JFXListCell<CurrencyMarket> call(JFXListView<CurrencyMarket> p) {

                JFXListCell<CurrencyMarket> cell = new JFXListCell<>(){

                    @Override
                    protected void updateItem(CurrencyMarket currencyMarket, boolean bln) {
                        super.updateItem(currencyMarket, bln);
                    }

                };

                cell.setOnMouseClicked(e -> {
                    try {
                        selectItem();
                    } catch (Exception err) {

                    }
                });

                return cell;
            }

        });

    }
    public void selectItem() throws Exception {
        CurrencyMarket currencyMarket = (CurrencyMarket) menu.getSelectionModel().getSelectedItem();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../../Views/CurrencyMarket/CurrencyMarketShow.fxml"));
        content.getChildren().setAll((VBox) loader.load());
        CurrencyMarketShow controller = loader.<CurrencyMarketShow>getController();
        controller.initData(currencyMarket);
    }

    public void navigateToCreate(MouseEvent event) throws Exception {
        StackPane createLayout = FXMLLoader.load(getClass().getResource("../../Views/CurrencyMarket/CurrencyMarketCreate.fxml"));
        Main.MainWindowController.replaceTabContent("CurrencyMarket", createLayout);
    }
}
