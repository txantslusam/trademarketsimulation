package com.tradeMarketSimulation.App.Controllers.Funds;

import com.jfoenix.controls.JFXListCell;
import com.jfoenix.controls.JFXListView;
import com.tradeMarketSimulation.App.Main;
import com.tradeMarketSimulation.Core.agents.Fund;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import java.net.URL;
import java.util.ResourceBundle;

public class FundsIndex implements Initializable {
    @FXML
    public JFXListView menu;
    @FXML
    public VBox content;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<Fund> fundObservableList = FXCollections.observableList(Main.simulatorInstance.funds);
        menu.setItems(fundObservableList);

        menu.setCellFactory(new Callback<JFXListView<Fund>, JFXListCell<Fund>>(){

            @Override
            public JFXListCell<Fund> call(JFXListView<Fund> p) {

                JFXListCell<Fund> cell = new JFXListCell<>(){

                    @Override
                    protected void updateItem(Fund fund, boolean bln) {
                        super.updateItem(fund, bln);
                    }

                };

                cell.setOnMouseClicked(e -> {
                    try {
                        selectItem();
                    } catch (Exception err) {

                    }
                });

                return cell;
            }

        });

    }
    public void selectItem() throws Exception {
        Fund fund = (Fund) menu.getSelectionModel().getSelectedItem();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../../Views/Funds/FundsShow.fxml"));
        content.getChildren().setAll((VBox) loader.load());
        FundsShow controller = loader.<FundsShow>getController();
        controller.initData(fund);
    }

    public void navigateToCreate(MouseEvent event) throws Exception {
        StackPane createLayout = FXMLLoader.load(getClass().getResource("../../Views/Funds/FundsCreate.fxml"));
        Main.MainWindowController.replaceTabContent("Fund", createLayout);
    }
}
