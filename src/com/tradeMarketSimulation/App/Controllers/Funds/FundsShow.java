package com.tradeMarketSimulation.App.Controllers.Funds;

import com.tradeMarketSimulation.App.Main;
import com.tradeMarketSimulation.Core.agents.Fund;
import com.tradeMarketSimulation.Core.helpers.Utils;
import com.tradeMarketSimulation.Core.tradeItems.TradeItem;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.net.URL;
import java.util.ConcurrentModificationException;
import java.util.ResourceBundle;

public class FundsShow implements Initializable {
    private Fund fund;
    @FXML
    Label name;
    @FXML
    Label firstName;
    @FXML
    Label lastName;
    @FXML
    Label budget;
    @FXML
    Label currency;
    @FXML
    TableView tradeItems;
    @FXML
    Label deletePrompt;

    public void initData(Fund fund) {
        this.fund = fund;
        name.textProperty().setValue(fund.getName());
        firstName.textProperty().setValue(fund.getManagerFirstName());
        lastName.textProperty().setValue(fund.getManagerLastName());
        currency.textProperty().setValue(fund.getUsedCurrency().toString());
        budget.textProperty().setValue(Utils.formatDecimalToFourPlaces(fund.getBudget()));
        fund.budgetProperty().addListener((observable, oldValue, newValue) -> {
            budget.textProperty().setValue(Utils.formatDecimalToFourPlaces(newValue));
        });
        initTradeItemsTable(fund.getTradeItems());
    }

    private void initTradeItemsTable(ObservableList<TradeItem> tradeItems) {
        this.tradeItems.setItems(tradeItems);
        TableColumn<TradeItem, String> col = new TableColumn<>("Trade Items");
        this.tradeItems.maxWidth(200);
        col.setPrefWidth(200);
        col.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getName()));
        this.tradeItems.getColumns().setAll(col);
    }

    public void deleteFund() throws IOException {
        try {
            Main.simulatorInstance.deleteFund(this.fund);
            BorderPane index = FXMLLoader.load(getClass().getResource("../../Views/Funds/FundsIndex.fxml"));
            Main.MainWindowController.replaceTabContent("Fund", index);
        } catch (ConcurrentModificationException e) {
            deletePrompt.setText("Wooops, something went wrong.");
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
