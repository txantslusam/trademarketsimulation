package com.tradeMarketSimulation.App.Controllers.Investors;

import com.jfoenix.controls.JFXListCell;
import com.jfoenix.controls.JFXListView;
import com.tradeMarketSimulation.App.Main;
import com.tradeMarketSimulation.Core.agents.Investor;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import java.net.URL;
import java.util.ResourceBundle;

public class InvestorsIndex implements Initializable {
    @FXML
    public JFXListView menu;
    @FXML
    public VBox content;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<Investor> investorObservableList = FXCollections.observableList(Main.simulatorInstance.investors);
        menu.setItems(investorObservableList);

        menu.setCellFactory(new Callback<JFXListView<Investor>, JFXListCell<Investor>>(){

            @Override
            public JFXListCell<Investor> call(JFXListView<Investor> p) {

                JFXListCell<Investor> cell = new JFXListCell<>(){

                    @Override
                    protected void updateItem(Investor investor, boolean bln) {
                        super.updateItem(investor, bln);
                    }

                };

                cell.setOnMouseClicked(e -> {
                    try {
                        selectItem();
                    } catch (Exception err) {

                    }
                });

                return cell;
            }

        });

    }
    public void selectItem() throws Exception {
        Investor investor = (Investor) menu.getSelectionModel().getSelectedItem();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../../Views/Investors/InvestorsShow.fxml"));
        content.getChildren().setAll((VBox) loader.load());
        InvestorsShow controller = loader.<InvestorsShow>getController();
        controller.initData(investor);
    }

    public void navigateToCreate(MouseEvent event) throws Exception {
        StackPane createLayout = FXMLLoader.load(getClass().getResource("../../Views/Investors/InvestorsCreate.fxml"));
        Main.MainWindowController.replaceTabContent("Investor", createLayout);
    }
}
