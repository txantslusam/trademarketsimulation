package com.tradeMarketSimulation.App.Controllers.Investors;

import com.tradeMarketSimulation.App.Main;
import com.tradeMarketSimulation.Core.agents.Investor;
import com.tradeMarketSimulation.Core.helpers.Utils;
import com.tradeMarketSimulation.Core.tradeItems.TradeItem;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.net.URL;
import java.util.ConcurrentModificationException;
import java.util.ResourceBundle;

public class InvestorsShow implements Initializable {
    private Investor investor;
    @FXML
    Label firstName;
    @FXML
    Label lastName;
    @FXML
    Label pesel;
    @FXML
    Label budget;
    @FXML
    Label currency;
    @FXML
    TableView tradeItems;
    @FXML
    Label deletePrompt;

    public void initData(Investor investor) {
        this.investor = investor;
        firstName.textProperty().setValue(investor.getFirstName());
        lastName.textProperty().setValue(investor.getLastName());
        pesel.textProperty().setValue(investor.getPESEL());
        currency.textProperty().setValue(investor.getUsedCurrency().toString());
        budget.textProperty().setValue(Utils.formatDecimalToFourPlaces(investor.getBudget()));
        investor.budgetProperty().addListener((observable, oldValue, newValue) -> {
            budget.textProperty().setValue(Utils.formatDecimalToFourPlaces(newValue));
        });
        initTradeItemsTable(investor.getTradeItems());
    }

    private void initTradeItemsTable(ObservableList<TradeItem> tradeItems) {
        this.tradeItems.setItems(tradeItems);
        TableColumn<TradeItem, String> col = new TableColumn<>("Trade Items");
        this.tradeItems.maxWidth(200);
        col.setPrefWidth(200);
        col.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getName()));
        this.tradeItems.getColumns().setAll(col);
    }

    public void deleteInvestor() throws IOException {
        try {
            Main.simulatorInstance.deleteInvestor(this.investor);
            BorderPane index = FXMLLoader.load(getClass().getResource("../../Views/Investors/InvestorsIndex.fxml"));
            Main.MainWindowController.replaceTabContent("Investor", index);
        } catch (ConcurrentModificationException e) {
            deletePrompt.setText("Wooops, something went wrong.");
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
