package com.tradeMarketSimulation.App.Controllers.Investors;

import com.github.javafaker.Faker;
import com.jfoenix.controls.*;
import com.tradeMarketSimulation.App.Main;
import com.tradeMarketSimulation.Core.Exceptions.MissingRequiredFieldException;
import com.tradeMarketSimulation.Core.agents.Investor;
import com.tradeMarketSimulation.Core.tradeItems.Currency;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

public class InvestorsCreate implements Initializable {
    @FXML
    StackPane container;
    @FXML
    JFXTextField firstName;
    @FXML
    Label firstNameValidation;
    @FXML
    JFXTextField lastName;
    @FXML
    Label lastNameValidation;
    @FXML
    JFXTextField pesel;
    @FXML
    Label peselValidation;
    @FXML
    JFXComboBox<Currency> currency;
    @FXML
    Label currencyValidator;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        firstName.setText(Faker.instance().name().firstName());
        lastName.setText(Faker.instance().name().lastName());
        pesel.setText(Faker.instance().number().digits(12));
        ObservableList<Currency> currencyObservableList = FXCollections.observableList(Main.simulatorInstance.currencies);
        currency.setItems(currencyObservableList);
        currency.getSelectionModel().select((int) (Math.random() * currencyObservableList.size()));
    }

    private void checkValidation() throws MissingRequiredFieldException {
        resetValidationFields();
        boolean isInvalid = false;
        if(firstName.getText().length() < 1) {
            firstNameValidation.setText("First name is required");
            isInvalid = true;
        }
        if(lastName.getText().length() < 1) {
            lastNameValidation.setText("Last name is required");
            isInvalid = true;
        }
        if(pesel.getText().length() < 1) {
            peselValidation.setText("PESEL is required");
            isInvalid = true;
        }
        if(currency.getSelectionModel().getSelectedIndex() == -1) {
            currencyValidator.setText("Stock exchange is required");
            isInvalid = true;
        }
        if(isInvalid) {
            throw new MissingRequiredFieldException();
        }
    }

    private void resetValidationFields() {
        firstNameValidation.setText("");
        lastNameValidation.setText("");
        peselValidation.setText("");
        currencyValidator.setText("");
    }

    private void spawnDialog() {
        JFXDialogLayout content = new JFXDialogLayout();
        content.setHeading(new Text("Successfully created Investor"));
        JFXDialog dialog = new JFXDialog(container, content, JFXDialog.DialogTransition.CENTER);
        JFXButton button = new JFXButton("Okey");
        button.setOnAction(event -> {
            try {
                dialog.close();
                goBack();
            } catch (Exception e) {

            }
        });
        content.setActions(button);
        dialog.show();
    }

    public void saveInvestor() {
        try {
            checkValidation();
            Main.simulatorInstance.createNewInvestor(new Investor(
                    Main.simulatorInstance,
                    Math.random() * 1000,
                    currency.getSelectionModel().getSelectedItem(),
                    firstName.getText(),
                    lastName.getText(),
                    pesel.getText()
            ));
            spawnDialog();
        } catch (MissingRequiredFieldException e) {
            return;
        }
    }

    public void goBack() throws Exception {
        BorderPane index = FXMLLoader.load(getClass().getResource("../../Views/Investors/InvestorsIndex.fxml"));
        Main.MainWindowController.replaceTabContent("Investor", index);
    }
}
