package com.tradeMarketSimulation.App.Controllers.StockExchanges;

import com.tradeMarketSimulation.App.Main;
import com.tradeMarketSimulation.Core.Exceptions.NoMoreTradeMarketException;
import com.tradeMarketSimulation.Core.company.Company;
import com.tradeMarketSimulation.Core.tradeMarkets.Exchange;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.ResourceBundle;

public class StockExchangesShow implements Initializable {
    private Exchange exchange;
    @FXML
    Label name;
    @FXML
    Label country;
    @FXML
    Label city;
    @FXML
    Label address;
    @FXML
    Label currency;
    @FXML
    Label transactionMargin;
    @FXML
    TableView companies;
    @FXML
    Label deletePrompt;

    public void initData(Exchange exchange) {
        this.exchange = exchange;
        name.textProperty().setValue(exchange.getName());
        country.textProperty().setValue(exchange.getCountry().getName());
        city.textProperty().setValue(exchange.getCity());
        address.textProperty().setValue(exchange.getAddress());
        currency.textProperty().setValue(exchange.getCurrency().getName());
        transactionMargin.textProperty().setValue(String.valueOf(exchange.getTransactionMargin()));
        initCompaniesTable(exchange.getCompanies());
    }

    private void initCompaniesTable(ArrayList<Company> companies) {
        ObservableList<Company> companyObservableList = FXCollections.observableList(companies);
        this.companies.setItems(companyObservableList);
        TableColumn<Company, String> col = new TableColumn<>("Company");
        this.companies.maxWidth(200);
        col.setPrefWidth(200);
        col.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getName()));
        this.companies.getColumns().setAll(col);
    }

    public void deleteExchange() throws Exception {
        try {
            Main.simulatorInstance.deleteStockExchange(this.exchange);
            BorderPane StockExchangeIndex = FXMLLoader.load(getClass().getResource("../../Views/StockExchanges/StockExchangesIndex.fxml"));
            Main.MainWindowController.replaceTabContent("StockExchange", StockExchangeIndex);
        } catch (NoMoreTradeMarketException e) {
            deletePrompt.setText("You can't remove last market");
        } catch (ConcurrentModificationException e) {
            deletePrompt.setText("Wooops, something went wrong.");
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
