package com.tradeMarketSimulation.App.Controllers.StockExchanges;

import com.jfoenix.controls.JFXListCell;
import com.jfoenix.controls.JFXListView;
import com.tradeMarketSimulation.App.Main;
import com.tradeMarketSimulation.Core.tradeMarkets.Exchange;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import java.net.URL;
import java.util.ResourceBundle;

public class StockExchangesIndex implements Initializable {

    @FXML
    public JFXListView menu;
    @FXML
    public VBox content;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<Exchange> exchangeObservableList = FXCollections.observableList(Main.simulatorInstance.exchanges);
        menu.setItems(exchangeObservableList);

        menu.setCellFactory(new Callback<JFXListView<Exchange>, JFXListCell<Exchange>>(){

            @Override
            public JFXListCell<Exchange> call(JFXListView<Exchange> p) {

                JFXListCell<Exchange> cell = new JFXListCell<>(){

                    @Override
                    protected void updateItem(Exchange exchange, boolean bln) {
                        super.updateItem(exchange, bln);
                        if (exchange != null) {
                            setText(exchange.getName() + ", " + exchange.getCity());
                        }
                    }

                };

                cell.setOnMouseClicked(e -> {
                    try {
                        selectItem();
                    } catch (Exception err) {

                    }
                });

                return cell;
            }

        });

    }
    public void selectItem() throws Exception {
        Exchange exchange = (Exchange) menu.getSelectionModel().getSelectedItem();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../../Views/StockExchanges/StockExchangesShow.fxml"));
        content.getChildren().setAll((VBox) loader.load());
        StockExchangesShow controller = loader.<StockExchangesShow>getController();
        controller.initData(exchange);
    }

    public void navigateToCreate(MouseEvent event) throws Exception {
        StackPane createLayout = FXMLLoader.load(getClass().getResource("../../Views/StockExchanges/StockExchangesCreate.fxml"));
        Main.MainWindowController.replaceTabContent("StockExchange", createLayout);
    }
}
