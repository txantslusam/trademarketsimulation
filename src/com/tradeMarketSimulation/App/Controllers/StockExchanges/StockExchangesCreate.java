package com.tradeMarketSimulation.App.Controllers.StockExchanges;

import com.github.javafaker.Faker;
import com.jfoenix.controls.*;
import com.tradeMarketSimulation.App.Main;
import com.tradeMarketSimulation.Core.Exceptions.MissingRequiredFieldException;
import com.tradeMarketSimulation.Core.simulationUtlis.Country;
import com.tradeMarketSimulation.Core.tradeMarkets.Exchange;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

public class StockExchangesCreate implements Initializable {
    @FXML
    StackPane container;
    @FXML
    JFXComboBox<Country> country;
    @FXML
    JFXTextField name;
    @FXML
    JFXTextField city;
    @FXML
    JFXTextField address;
    @FXML
    Label cityValidation;
    @FXML
    Label addressValidation;
    @FXML
    Label countryValidation;
    @FXML
    Label nameValidation;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<Country> countryObservableList = FXCollections.observableList(Main.simulatorInstance.countries);
        country.setItems(countryObservableList);
        name.setText(Faker.instance().harryPotter().location());
        city.setText(Faker.instance().address().city());
        address.setText(Faker.instance().address().streetAddress());
        country.getSelectionModel().select((int) Math.floor(Math.random() * countryObservableList.size()));
    }

    private void checkValidation() throws MissingRequiredFieldException {
        resetValidationFields();
        boolean isInvalid = false;
        if(name.getText().length() < 1) {
            nameValidation.setText("Name is required");
            isInvalid = true;
        }
        if(city.getText().length() < 1) {
            cityValidation.setText("City is required");
            isInvalid = true;
        }
        if(address.getText().length() < 1) {
            addressValidation.setText("Address is required");
            isInvalid = true;
        }
        if(country.getSelectionModel().getSelectedIndex() == -1) {
            countryValidation.setText("Country is required");
            isInvalid = true;
        }
        if(isInvalid) {
            throw new MissingRequiredFieldException();
        }
    }

    private void resetValidationFields() {
        nameValidation.setText("");
        cityValidation.setText("");
        addressValidation.setText("");
        countryValidation.setText("");
    }

    private void spawnDialog() {
        JFXDialogLayout content = new JFXDialogLayout();
        content.setHeading(new Text("Successfully created Stock Exchange"));
        JFXDialog dialog = new JFXDialog(container, content, JFXDialog.DialogTransition.CENTER);
        JFXButton button = new JFXButton("Okey");
        button.setOnAction(event -> {
            try {
                dialog.close();
                goBack();
            } catch (Exception e) {

            }
        });
        content.setActions(button);
        dialog.show();
    }

    public void saveExchange() {
        try {
            checkValidation();
            Main.simulatorInstance.createNewExchange(new Exchange(
                    name.getText(),
                    Math.random(),
                    country.getSelectionModel().getSelectedItem(),
                    city.getText(),
                    address.getText(),
                    country.getSelectionModel().getSelectedItem().getCurrency()
            ));
            spawnDialog();
        } catch (MissingRequiredFieldException e) {
            return;
        }
    }

    public void goBack() throws Exception {
        BorderPane StockExchangeIndex = FXMLLoader.load(getClass().getResource("../../Views/StockExchanges/StockExchangesIndex.fxml"));
        Main.MainWindowController.replaceTabContent("StockExchange", StockExchangeIndex);
    }

}
