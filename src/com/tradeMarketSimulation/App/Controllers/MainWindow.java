package com.tradeMarketSimulation.App.Controllers;

import com.tradeMarketSimulation.Core.Simulation;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;

public class MainWindow implements Initializable{

    @FXML
    private AnchorPane stockExchangeTab;
    @FXML
    private AnchorPane currencyTab;
    @FXML
    private AnchorPane materialTab;
    @FXML
    private AnchorPane materialMarketTab;
    @FXML
    private AnchorPane currencyMarketTab;
    @FXML
    private AnchorPane companyTab;
    @FXML
    private AnchorPane indexTab;
    @FXML
    private AnchorPane investorTab;
    @FXML
    private AnchorPane fundTab;
    @FXML
    private Label dateIndicator;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        DateFormat dateFormat = new SimpleDateFormat("DD.MM.YYYY HH:mm");
        dateIndicator.setText(dateFormat.format(Simulation.simulationDate.get()));
        Simulation.simulationDate.addListener(((observable, oldValue, newValue) -> {
            dateIndicator.setText(dateFormat.format(newValue));
        }));
    }

    public void replaceTabContent(String tabName, Node tabContent) {
        switch (tabName) {
            case "StockExchange":
                stockExchangeTab.getChildren().setAll(tabContent);
                break;
            case "MaterialMarket":
                materialMarketTab.getChildren().setAll(tabContent);
                break;
            case "CurrencyMarket":
                currencyMarketTab.getChildren().setAll(tabContent);
                break;
            case "Currency":
                currencyTab.getChildren().setAll(tabContent);
                break;
            case "Company":
                companyTab.getChildren().setAll(tabContent);
                break;
            case "Material":
                materialTab.getChildren().setAll(tabContent);
                break;
            case "Index":
                indexTab.getChildren().setAll(tabContent);
                break;
            case "Investor":
                investorTab.getChildren().setAll(tabContent);
                break;
            case "Fund":
                fundTab.getChildren().setAll(tabContent);
                break;
        }
    }
}
