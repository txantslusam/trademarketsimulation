package com.tradeMarketSimulation.App.Controllers.MaterialMarket;

import com.jfoenix.controls.JFXListCell;
import com.jfoenix.controls.JFXListView;
import com.tradeMarketSimulation.App.Main;
import com.tradeMarketSimulation.Core.tradeMarkets.MaterialMarket;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import java.net.URL;
import java.util.ResourceBundle;

public class MaterialMarketIndex implements Initializable {
    @FXML
    public JFXListView menu;
    @FXML
    public VBox content;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<MaterialMarket> materialMarketObservableList = FXCollections.observableList(Main.simulatorInstance.materialMarkets);
        menu.setItems(materialMarketObservableList);

        menu.setCellFactory(new Callback<JFXListView<MaterialMarket>, JFXListCell<MaterialMarket>>(){

            @Override
            public JFXListCell<MaterialMarket> call(JFXListView<MaterialMarket> p) {

                JFXListCell<MaterialMarket> cell = new JFXListCell<>(){

                    @Override
                    protected void updateItem(MaterialMarket materialMarket, boolean bln) {
                        super.updateItem(materialMarket, bln);
                    }

                };

                cell.setOnMouseClicked(e -> {
                    try {
                        selectItem();
                    } catch (Exception err) {

                    }
                });

                return cell;
            }

        });

    }
    public void selectItem() throws Exception {
        MaterialMarket materialMarket = (MaterialMarket) menu.getSelectionModel().getSelectedItem();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../../Views/MaterialMarket/MaterialMarketShow.fxml"));
        content.getChildren().setAll((VBox) loader.load());
        MaterialMarketShow controller = loader.<MaterialMarketShow>getController();
        controller.initData(materialMarket);
    }

    public void navigateToCreate(MouseEvent event) throws Exception {
        StackPane createLayout = FXMLLoader.load(getClass().getResource("../../Views/MaterialMarket/MaterialMarketCreate.fxml"));
        Main.MainWindowController.replaceTabContent("MaterialMarket", createLayout);
    }

}
