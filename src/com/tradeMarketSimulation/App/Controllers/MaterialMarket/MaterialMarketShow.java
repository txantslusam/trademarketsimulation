package com.tradeMarketSimulation.App.Controllers.MaterialMarket;

import com.tradeMarketSimulation.App.Main;
import com.tradeMarketSimulation.Core.Exceptions.NoMoreTradeMarketException;
import com.tradeMarketSimulation.Core.helpers.Utils;
import com.tradeMarketSimulation.Core.tradeItems.Material;
import com.tradeMarketSimulation.Core.tradeMarkets.MaterialMarket;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.ResourceBundle;

public class MaterialMarketShow implements Initializable {
    private MaterialMarket materialMarket;
    @FXML
    Label name;
    @FXML
    Label transactionMargin;
    @FXML
    TableView materials;
    @FXML
    Label deletePrompt;

    public void initData(MaterialMarket materialMarket) {
        this.materialMarket = materialMarket;
        name.textProperty().setValue(materialMarket.getName());
        transactionMargin.textProperty().setValue(Utils.formatDecimalToFourPlaces(materialMarket.getTransactionMargin()));
        initMaterialsTable(materialMarket.getMaterials());
    }

    private void initMaterialsTable(ArrayList<Material> materials) {
        ObservableList<Material> materialObservableList = FXCollections.observableList(materials);
        this.materials.setItems(materialObservableList);
        TableColumn<Material, String> col = new TableColumn<>("Material");
        this.materials.maxWidth(200);
        col.setPrefWidth(200);
        col.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getName()));
        this.materials.getColumns().setAll(col);
    }

    public void deleteMaterialMarket() throws IOException {
        try {
            Main.simulatorInstance.deleteMaterialMarket(this.materialMarket);
            BorderPane index = FXMLLoader.load(getClass().getResource("../../Views/MaterialMarket/MaterialMarketIndex.fxml"));
            Main.MainWindowController.replaceTabContent("MaterialMarket", index);
        } catch (NoMoreTradeMarketException e) {
            deletePrompt.setText("You can't remove last market");
        } catch (ConcurrentModificationException e) {
            deletePrompt.setText("Wooops, something went wrong.");
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
