package com.tradeMarketSimulation.App.Controllers.MaterialMarket;

import com.github.javafaker.Faker;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextField;
import com.tradeMarketSimulation.App.Main;
import com.tradeMarketSimulation.Core.Exceptions.MissingRequiredFieldException;
import com.tradeMarketSimulation.Core.tradeMarkets.MaterialMarket;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

public class MaterialMarketCreate implements Initializable {
    @FXML
    StackPane container;
    @FXML
    JFXTextField name;
    @FXML
    Label nameValidation;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        name.setText(Faker.instance().witcher().location());
    }

    private void checkValidation() throws MissingRequiredFieldException {
        resetValidationFields();
        boolean isInvalid = false;
        if(name.getText().length() < 1) {
            nameValidation.setText("Name is required");
            isInvalid = true;
        }
        if(isInvalid) {
            throw new MissingRequiredFieldException();
        }
    }

    private void resetValidationFields() {
        nameValidation.setText("");
    }

    private void spawnDialog() {
        JFXDialogLayout content = new JFXDialogLayout();
        content.setHeading(new Text("Successfully created Material Market"));
        JFXDialog dialog = new JFXDialog(container, content, JFXDialog.DialogTransition.CENTER);
        JFXButton button = new JFXButton("Okey");
        button.setOnAction(event -> {
            try {
                dialog.close();
                goBack();
            } catch (Exception e) {

            }
        });
        content.setActions(button);
        dialog.show();
    }

    public void saveMaterialMarket() {
        try {
            checkValidation();
            Main.simulatorInstance.createNewMaterialMarket(new MaterialMarket(
                    name.getText(),
                    Math.random()
            ));
            spawnDialog();
        } catch (MissingRequiredFieldException e) {
            return;
        }
    }

    public void goBack() throws Exception {
        BorderPane index = FXMLLoader.load(getClass().getResource("../../Views/MaterialMarket/MaterialMarketIndex.fxml"));
        Main.MainWindowController.replaceTabContent("MaterialMarket", index);
    }
}
