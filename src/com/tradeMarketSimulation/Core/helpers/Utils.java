package com.tradeMarketSimulation.Core.helpers;

import java.text.DecimalFormat;

public class Utils {
    public static String formatDecimalToFourPlaces(Number value) {
        DecimalFormat df = new DecimalFormat("#.####");
        return String.valueOf(df.format(value));
    }
}
