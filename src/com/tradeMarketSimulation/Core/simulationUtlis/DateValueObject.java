package com.tradeMarketSimulation.Core.simulationUtlis;

import java.util.Date;

public class DateValueObject<X> {
    private Date date;
    private X value;

    public DateValueObject(Date date, X value) {
        this.date = date;
        this.value = value;
    }

    public Date getDate() {
        return date;
    }

    public X getValue() {
        return value;
    }
}
