package com.tradeMarketSimulation.Core.simulationUtlis;

import com.tradeMarketSimulation.Core.tradeItems.Currency;

public class Country {
    private String name;
    private Currency currency;

    public Country(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return name;
    }
}
