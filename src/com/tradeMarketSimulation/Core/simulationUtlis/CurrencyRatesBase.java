package com.tradeMarketSimulation.Core.simulationUtlis;

import com.tradeMarketSimulation.Core.tradeItems.Currency;

public class CurrencyRatesBase {

    public static double calculateExchangeRate(Currency ownCurrency, Currency targetCurrency) {
        return ownCurrency.getActualValue() / targetCurrency.getActualValue();
    }
}