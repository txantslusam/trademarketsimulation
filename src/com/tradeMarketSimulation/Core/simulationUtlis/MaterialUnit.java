package com.tradeMarketSimulation.Core.simulationUtlis;

import java.util.ArrayList;

public class MaterialUnit {
    private static ArrayList<String> materialUnits = new ArrayList<>() {{
        add("ounce");
        add("liter");
        add("kilogram");
        add("gram");
        add("piece");
        add("galon");
        add("barrel");
        add("meter");
    }};

    public static ArrayList<String> getMaterialUnits() {
        return materialUnits;
    }

    public static String getMaterialUnitAtRandom() {
        return materialUnits.get((int) Math.floor(Math.random() * materialUnits.size()));
    }
}
