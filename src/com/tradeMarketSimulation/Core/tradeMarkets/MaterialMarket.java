package com.tradeMarketSimulation.Core.tradeMarkets;

import com.tradeMarketSimulation.Core.Exceptions.NoMaterialException;
import com.tradeMarketSimulation.Core.tradeItems.Material;

import java.util.ArrayList;

public class MaterialMarket extends TradeMarket {
    private ArrayList<Material> materials = new ArrayList<>();

    public ArrayList<Material> getMaterials() {
        return materials;
    }

    public void addNewMaterial(Material material) {
        if (!this.materials.contains(material)) {
            this.materials.add(material);
        }
    }

    public MaterialMarket(String name, double transactionMargin) {
        super(name, transactionMargin);
    }

    public Material getRandomMaterial() throws NoMaterialException {
        if(materials.size() > 0) {
            return materials.get((int) (Math.random() * materials.size()));
        } else {
            throw new NoMaterialException();
        }
    }

    public void detachMaterial(Material material) {
        materials.remove(material);
    }

}
