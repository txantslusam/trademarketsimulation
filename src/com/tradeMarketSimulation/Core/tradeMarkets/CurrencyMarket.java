package com.tradeMarketSimulation.Core.tradeMarkets;

import com.tradeMarketSimulation.Core.Exceptions.NoCurrencyException;
import com.tradeMarketSimulation.Core.tradeItems.Currency;

import java.util.ArrayList;

public class CurrencyMarket extends TradeMarket {
    private ArrayList<Currency> currencies = new ArrayList<>();

    public ArrayList<Currency> getCurrencies() {
        return currencies;
    }

    public void addNewCurrency(Currency currency) {
        this.currencies.add(currency);
    }

    public CurrencyMarket(String name, double transactionMargin) {
        super(name, transactionMargin);
    }

    public Currency getRandomCurrency() throws NoCurrencyException {
        if(currencies.size() > 0) {
            return currencies.get((int) (Math.random() * currencies.size()));
        } else {
            throw new NoCurrencyException();
        }
    }

    public void detachCurrency(Currency currency) {
        currencies.remove(currency);
    }
}
