package com.tradeMarketSimulation.Core.tradeMarkets;

public class TradeMarket {
    private String name;
    private double transactionMargin;

    public String getName() {
        return name;
    }

    public double getTransactionMargin() {
        return transactionMargin;
    }

    public TradeMarket(String name, double transactionMargin) {
        this.name = name;
        this.transactionMargin = transactionMargin;
    }

    @Override
    public String toString() {
        return name;
    }
}
