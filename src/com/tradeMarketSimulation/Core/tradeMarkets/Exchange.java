package com.tradeMarketSimulation.Core.tradeMarkets;

import com.tradeMarketSimulation.Core.Exceptions.NoCompanyException;
import com.tradeMarketSimulation.Core.company.Company;
import com.tradeMarketSimulation.Core.simulationUtlis.Country;
import com.tradeMarketSimulation.Core.tradeItems.Currency;

import java.util.ArrayList;

public class Exchange extends TradeMarket {
    private Country country;
    private String city;
    private String address;
    private Currency currency;
    private ArrayList<Company> companies = new ArrayList<>();

    public ArrayList<Company> getCompanies() {
        return companies;
    }

    public void addNewCompany(Company company) {
        if (!this.companies.contains(company)) {
            this.companies.add(company);
        }
    }

    public Country getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Exchange(String name, double transactionMargin, Country country, String city, String address, Currency currency) {
        super(name, transactionMargin);
        this.country = country;
        this.city = city;
        this.address = address;
        this.currency = currency;
    }

    public Company getRandomCompany() throws NoCompanyException {
        if(companies.size() > 0) {
            return companies.get((int) (Math.random() * companies.size()));
        } else {
            throw new NoCompanyException();
        }
    }

    public void detachCompany(Company company) {
        companies.remove(company);
    }
}
