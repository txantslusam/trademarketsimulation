package com.tradeMarketSimulation.Core.company;

import com.tradeMarketSimulation.Core.tradeItems.TradeItem;
import com.tradeMarketSimulation.Core.tradeMarkets.Exchange;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

import java.util.Timer;
import java.util.TimerTask;

public class Company extends TradeItem implements Runnable {
    private volatile IntegerProperty stockCount = new SimpleIntegerProperty();
    private DoubleProperty profit = new SimpleDoubleProperty();
    private DoubleProperty income = new SimpleDoubleProperty();
    private DoubleProperty equityCapital = new SimpleDoubleProperty();
    private DoubleProperty shareCapital = new SimpleDoubleProperty();
    private volatile IntegerProperty volume = new SimpleIntegerProperty();
    private volatile DoubleProperty sales = new SimpleDoubleProperty();
    private transient Thread th;

    public Thread getTh() {
        return th;
    }

    public void setTh(Thread th) {
        this.th = th;
    }

    public int getStockCount() {
        return stockCount.get();
    }

    public IntegerProperty stockCountProperty() {
        return stockCount;
    }

    public double getProfit() {
        return profit.get();
    }

    public DoubleProperty profitProperty() {
        return profit;
    }

    public double getIncome() {
        return income.get();
    }

    public DoubleProperty incomeProperty() {
        return income;
    }

    public double getEquityCapital() {
        return equityCapital.get();
    }

    public DoubleProperty equityCapitalProperty() {
        return equityCapital;
    }

    public double getShareCapital() {
        return shareCapital.get();
    }

    public DoubleProperty shareCapitalProperty() {
        return shareCapital;
    }

    public int getVolume() {
        return volume.get();
    }

    public IntegerProperty volumeProperty() {
        return volume;
    }

    public double getSales() {
        return sales.get();
    }

    public DoubleProperty salesProperty() {
        return sales;
    }

    public Company(String name, double actualValue, Exchange exchange) {
        super(name, actualValue, exchange);
        stockCount.setValue((int) Math.round(Math.random() * 500 + 100));
        equityCapital.setValue(Math.round(Math.random() * 1000 + 300));
        shareCapital.setValue(Math.round(Math.random() * 1000 + 300));
    }

    @Override
    public void run() {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(generateProfit(), 0L, 1000L);
        timer.scheduleAtFixedRate(makeNewStocks(), 0L, (long) (Math.random() * 5000 + 1000));
    }

    private TimerTask generateProfit() {
        return new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> profit.setValue(Math.random() * 400 - 100));
                Platform.runLater(Company.this::generateIncome);
            }
        };
    }

    private void generateIncome() {
        double newIncome;
        if (profit.get() < 0) {
            newIncome = profit.get();
        } else {
            newIncome = profit.get() - profit.get() * (getPartnerTradeMarket().getTransactionMargin() / 10);
        }
        income.set(newIncome);
        equityCapital.set(equityCapital.get() + income.get());
    }

    private TimerTask makeNewStocks() {
        return new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> stockCount.setValue(stockCount.get() + (int) (Math.random() * 100)));
            }
        };
    }

    @Override
    public void buy() {
        synchronized (this) {
            super.buy();
            stockCount.set(stockCount.get() - 1);
            volume.set(volume.get() + 1);
            sales.set(sales.get() + getBuyPrice());
        }
    }

    @Override
    public void sell() {
        synchronized (this) {
            super.sell();
            stockCount.set(stockCount.get() + 1);
            volume.set(volume.get() + 1);
            sales.set(sales.get() + getSellPrice());
        }
    }

    public int buyoutStock(double buyoutPrice) {
        if (buyoutPrice > equityCapital.get() + shareCapital.get()) {
            buyoutPrice = equityCapital.get() + shareCapital.get();
        }

        int buyoutStockCount = (int) (buyoutPrice / getBuyPrice());

        if (stockCount.get() < buyoutStockCount) {
            buyoutStockCount = stockCount.get();
            double stocksBuyoutPrice = stockCount.get() * getBuyPrice();
        }

        stockCount.set(stockCount.get() - buyoutStockCount);

        if (equityCapital.get() < buyoutPrice) {
            double buyoutPartPrice = buyoutPrice - equityCapital.get();
            equityCapital.set(0);
            shareCapital.set(shareCapital.get() - buyoutPartPrice);
        } else {
            equityCapital.set(equityCapital.get() - buyoutPrice);
        }

        return buyoutStockCount;
    }

    public void swapExchange(Exchange newExchange) {
        synchronized (this) {
            this.setPartnerTradeMarket(newExchange);
            newExchange.addNewCompany(this);
        }
    }
}
