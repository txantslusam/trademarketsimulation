package com.tradeMarketSimulation.Core.tradeItems;

import com.tradeMarketSimulation.Core.Simulation;
import com.tradeMarketSimulation.Core.simulationUtlis.DateValueObject;
import com.tradeMarketSimulation.Core.tradeMarkets.TradeMarket;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Date;

public class TradeItem {

    private String name;
    private volatile DoubleProperty actualValue = new SimpleDoubleProperty();
    private volatile DoubleProperty minimalValue = new SimpleDoubleProperty();
    private volatile DoubleProperty maximumValue = new SimpleDoubleProperty();
    private volatile DoubleProperty buyPrice = new SimpleDoubleProperty();
    private volatile DoubleProperty sellPrice = new SimpleDoubleProperty();
    private ObservableList<DateValueObject<Double>> priceChangeHistory = FXCollections.observableArrayList();
    private Date createdDate;
    private TradeMarket partnerTradeMarket;

    protected volatile transient double buySeries = 1;
    protected volatile transient double sellSeries = 1;

    public TradeItem(String name, double actualValue, TradeMarket partnerTradeMarket) {
        this.name = name;
        this.actualValue.set(actualValue);
        this.minimalValue.set(actualValue);
        this.maximumValue.set(actualValue);
        this.buyPrice.set(actualValue + 0.13);
        this.sellPrice.set(actualValue - 0.13);
        this.partnerTradeMarket = partnerTradeMarket;
        createdDate = Simulation.simulationDate.getValue();
        priceChangeHistory.add(new DateValueObject(createdDate, actualValue));
        setDateChangeListener();
    }

    public String getName() {
        return name;
    }

    public double getActualValue() {
        return actualValue.get();
    }

    public double getMinimalValue() {
        return minimalValue.get();
    }

    public double getMaximumValue() {
        return maximumValue.get();
    }

    public double getBuyPrice() {
        return buyPrice.get();
    }

    public double getSellPrice() {
        return sellPrice.get();
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public ObservableList<DateValueObject<Double>> getPriceChangeHistory() {
        return priceChangeHistory;
    }

    public TradeMarket getPartnerTradeMarket() {
        return partnerTradeMarket;
    }

    public void setPartnerTradeMarket(TradeMarket partnerTradeMarket) {
        this.partnerTradeMarket = partnerTradeMarket;
    }

    public DoubleProperty ActualValue() {
        return actualValue;
    }

    public DoubleProperty MinimalValue() {
        return minimalValue;
    }

    public DoubleProperty MaximumValue() {
        return maximumValue;
    }

    public DoubleProperty BuyPrice() {
        return buyPrice;
    }

    public DoubleProperty SellPrice() {
        return sellPrice;
    }

    protected double getSellersToBuyersRatio() {
        return sellSeries / buySeries;
    }

    /**
     * Method to reevaluate elements, helper for setting prices for agents
     */
    private void adjustValue() {
        synchronized(this) {
            double rate = Math.random() * 0.1;
            if (determineIfRaiseValue()) {
                this.raiseValue(rate);
            } else {
                this.lowerValue(rate);
            }
            sellSeries = 1;
            buySeries = 1;
        }
    }

    private void lowerValue(double rate) {
        synchronized(this) {
            if (actualValue.get() - rate < 0) {
                actualValue.set(0.01);
                buyPrice.set(actualValue.get() + 0.13);
                sellPrice.set(sellPrice.get());
            } else {
                actualValue.set(actualValue.get() - rate);
                buyPrice.set(actualValue.get() + 0.13);
                sellPrice.set(sellPrice.get() - 0.13);
            }
        }
    }

    private void raiseValue(double rate) {
        synchronized(this) {
            actualValue.set(actualValue.get() + rate);
            buyPrice.set(actualValue.get() + 0.13);
            sellPrice.set(actualValue.get() - 0.13);
        }
    }

    public void buy() {
        synchronized (this) {
            buySeries++;
        }
    }

    public void sell() {
        synchronized (this) {
            sellSeries++;
        }
    }

    private void setDateChangeListener() {
        Simulation.simulationDate.addListener(((observable, oldValue, newValue) -> {
            adjustValue();
            writeValueToHistory();
        }));
    }

    private void writeValueToHistory() {
        priceChangeHistory.add(new DateValueObject(Simulation.simulationDate.getValue(), actualValue.get()));
        if (actualValue.get() < minimalValue.get()) {
            minimalValue.set(actualValue.get());
        }
        if (actualValue.get() > maximumValue.get()) {
            maximumValue.set(actualValue.get());
        }
    }

    /**
     * Determine random event or actual value is lower then limit and upper limit is not achieved
     * @return boolean canRaise
     */
    protected boolean determineIfRaiseValue() {
        return (getSellersToBuyersRatio() > 0.96
                || priceChangeHistory.get(0).getValue() - 0.6 > actualValue.get()
                || actualValue.get() < 0.1
                ) && priceChangeHistory.get(0).getValue() + 0.6 > actualValue.get();
    }

    @Override
    public String toString() {
        return name;
    }
}
