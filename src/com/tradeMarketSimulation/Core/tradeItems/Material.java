package com.tradeMarketSimulation.Core.tradeItems;

import com.tradeMarketSimulation.Core.tradeMarkets.MaterialMarket;

public class Material extends TradeItem {
    private String materialUnit;
    private Currency currency;

    public String getMaterialUnit() {
        return materialUnit;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Material(String name, double actualValue, String materialUnit, Currency currency, MaterialMarket materialMarket) {
        super(name, actualValue, materialMarket);
        this.materialUnit = materialUnit;
        this.currency = currency;
    }

    public void swapMaterialMarket(MaterialMarket materialMarket) {
        synchronized (this) {
            this.setPartnerTradeMarket(materialMarket);
            materialMarket.addNewMaterial(this);
        }
    }
}
