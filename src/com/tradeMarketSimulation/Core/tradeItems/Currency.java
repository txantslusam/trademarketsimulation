package com.tradeMarketSimulation.Core.tradeItems;

import com.tradeMarketSimulation.Core.simulationUtlis.Country;
import com.tradeMarketSimulation.Core.tradeMarkets.CurrencyMarket;

import java.util.ArrayList;

public class Currency extends TradeItem {
    private ArrayList<Country> countries = new ArrayList<>();

    public ArrayList<Country> getCountries() {
        return countries;
    }

    public void setCountries(ArrayList<Country> countries) {
        this.countries = countries;
    }

    public void addCountry(Country country) {
        countries.add(country);
    }

    public Currency(String name, double actualValue, CurrencyMarket currencyMarket) {
        super(name, actualValue, currencyMarket);
    }

    public void swapCurrencyMarket(CurrencyMarket currencyMarket) {
        synchronized (this) {
            this.setPartnerTradeMarket(currencyMarket);
            currencyMarket.addNewCurrency(this);
        }
    }
}
