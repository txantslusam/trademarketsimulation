package com.tradeMarketSimulation.Core.agents;

import com.tradeMarketSimulation.Core.Simulation;
import com.tradeMarketSimulation.Core.simulationUtlis.CurrencyRatesBase;
import com.tradeMarketSimulation.Core.tradeItems.Currency;
import javafx.application.Platform;

import java.util.TimerTask;

public class Investor extends Agent {
    private String firstName;
    private String lastName;
    private String PESEL;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPESEL() {
        return PESEL;
    }

    public Investor(Simulation simulationInstance, double budget, Currency usedCurrency, String firstName, String lastName, String PESEL) {
        super(simulationInstance, budget, usedCurrency);
        this.firstName = firstName;
        this.lastName = lastName;
        this.PESEL = PESEL;
    }

    @Override
    public void run() {
        super.run();
        timer.scheduleAtFixedRate(addBudget(), 0L, (long) (Math.random() * 5000 + 1000));
        timer.scheduleAtFixedRate(buyFunds(), 0L, (long) (Math.random() * 10000 + 1000));
    }

    private TimerTask buyFunds() {
        return new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> {
                    Fund fund = Investor.this.simulationInstance.funds.get((int) (Math.random() * Investor.this.simulationInstance.funds.size()));
                    double moneyToInvest = Math.random() * 1000;
                    if (moneyToInvest > getBudget()) {
                        moneyToInvest = moneyToInvest - getBudget();
                    } else if (moneyToInvest > getBudget() / 2) {
                        moneyToInvest = moneyToInvest - getBudget() / 2;
                    }
                    setBudget(getBudget() - moneyToInvest);
                    fund.addMoney(CurrencyRatesBase.calculateExchangeRate(getUsedCurrency(), fund.getUsedCurrency()) * moneyToInvest);
                });
            }
        };
    }

    private TimerTask addBudget() {
        return new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> setBudget(getBudget() + Math.random() * 1000));
            }
        };
    }

    @Override
    public void goBankrupt() {
        super.goBankrupt();
        this.simulationInstance.investors.remove(this);
    }

    @Override
    public String toString() {
        return firstName + lastName;
    }
}
