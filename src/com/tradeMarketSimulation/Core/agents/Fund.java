package com.tradeMarketSimulation.Core.agents;

import com.tradeMarketSimulation.Core.Simulation;
import com.tradeMarketSimulation.Core.tradeItems.Currency;

public class Fund extends Agent {
    private String name;
    private String managerFirstName;
    private String managerLastName;

    public String getName() {
        return name;
    }

    public String getManagerFirstName() {
        return managerFirstName;
    }

    public String getManagerLastName() {
        return managerLastName;
    }

    public Fund(Simulation simulationInstance, double budget, Currency usedCurrency, String name, String managerFirstName, String managerLastName) {
        super(simulationInstance, budget, usedCurrency);
        this.name = name;
        this.managerFirstName = managerFirstName;
        this.managerLastName = managerLastName;
    }

    public void addMoney(double money) {
        setBudget(getBudget() + money);
    }

    @Override
    public void goBankrupt() {
        super.goBankrupt();
        this.simulationInstance.funds.remove(this);
    }

    @Override
    public String toString() {
        return name;
    }
}
