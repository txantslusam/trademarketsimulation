package com.tradeMarketSimulation.Core.agents;

import com.tradeMarketSimulation.Core.Exceptions.NoCompanyException;
import com.tradeMarketSimulation.Core.Exceptions.NoCurrencyException;
import com.tradeMarketSimulation.Core.Exceptions.NoMaterialException;
import com.tradeMarketSimulation.Core.Simulation;
import com.tradeMarketSimulation.Core.company.Company;
import com.tradeMarketSimulation.Core.simulationUtlis.CurrencyRatesBase;
import com.tradeMarketSimulation.Core.tradeItems.Currency;
import com.tradeMarketSimulation.Core.tradeItems.Material;
import com.tradeMarketSimulation.Core.tradeItems.TradeItem;
import com.tradeMarketSimulation.Core.tradeMarkets.CurrencyMarket;
import com.tradeMarketSimulation.Core.tradeMarkets.Exchange;
import com.tradeMarketSimulation.Core.tradeMarkets.MaterialMarket;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Timer;
import java.util.TimerTask;

public class Agent implements Runnable {
    protected Simulation simulationInstance;
    private DoubleProperty budget = new SimpleDoubleProperty();
    private ObservableList<TradeItem> tradeItems = FXCollections.observableArrayList();
    private Currency usedCurrency;
    Timer timer = new Timer();
    private Thread th;

    public double getBudget() {
        return budget.get();
    }

    public void setBudget(double budget) {
        this.budget.set(budget);
    }

    public DoubleProperty budgetProperty() {
        return budget;
    }

    public ObservableList<TradeItem> getTradeItems() {
        return tradeItems;
    }

    public Currency getUsedCurrency() {
        return usedCurrency;
    }

    public void setUsedCurrency(Currency usedCurrency) {
        this.usedCurrency = usedCurrency;
    }

    public void setTh(Thread th) {
        this.th = th;
    }

    public Agent(Simulation simulationInstance, double budget, Currency usedCurrency) {
        this.simulationInstance = simulationInstance;
        this.budget.set(budget);
        this.usedCurrency = usedCurrency;
    }

    @Override
    public void run() {
        timer.scheduleAtFixedRate(makeNewTransaction(), 0L, (long) (Math.random() * 60 + 30));
    }

    private TimerTask makeNewTransaction() {
        return new TimerTask() {
            @Override
            public void run() {
                double marketSelect = Math.random();
                double operationSelect = Math.random();
                if (budget.get() < 0) {
                    if(tradeItems.size() < 1) {
                        goBankrupt();
                        return;
                    } else {
                        operationSelect = 1;
                    }
                }
                if (marketSelect < 0.30) {
                    if (operationSelect < 0.5) {
                        Platform.runLater(() -> Agent.this.performBuyAction("Currency"));
                    } else {
                        Platform.runLater(() -> Agent.this.performSellAction("Currency"));
                    }
                } else if (marketSelect < 0.67) {
                    if (operationSelect < 0.5) {
                        Platform.runLater(() -> Agent.this.performBuyAction("Company"));
                    } else {
                        Platform.runLater(() -> Agent.this.performSellAction("Company"));
                    }
                } else {
                    if (operationSelect < 0.5) {
                        Platform.runLater(() -> Agent.this.performBuyAction("Material"));
                    } else {
                        Platform.runLater(() -> Agent.this.performSellAction("Material"));
                    }
                }
            }
        };
    }

    private void performBuyAction(String tradeItemType) {
        switch (tradeItemType) {
            case "Currency":
                performBuyCurrencyAction();
                break;
            case "Company":
                performBuyCompanyAction();
                break;
            case "Material":
                performBuyMaterialAction();
                break;
        }
    }

    private void performSellAction(String tradeItemType) {
        switch (tradeItemType) {
            case "Currency":
                performSellCurrencyAction();
                break;
            case "Company":
                performSellCompanyAction();
                break;
            case "Material":
                performSellMaterialAction();
                break;
        }
    }

    private void performBuyCurrencyAction() {
        CurrencyMarket currencyMarket = simulationInstance.getRandomCurrencyMarket();
        try {
            Currency currency = currencyMarket.getRandomCurrency();
            double currencyPrice = CurrencyRatesBase.calculateExchangeRate(this.usedCurrency, currency) * currency.getBuyPrice();
            if (currencyPrice > budget.get()) {
                return;
            } else {
                currency.buy();
                budget.set(budget.get() - currencyPrice);
                tradeItems.add(currency);
            }
        } catch (NoCurrencyException e) {
            return;
        }
    }

    private void performSellCurrencyAction() {
        Currency currency;
        for (TradeItem tradeItem : tradeItems) {
            if (tradeItem.getClass().getSimpleName().equals("Currency")) {
                currency = (Currency) tradeItem;
                double transactionValue = CurrencyRatesBase.calculateExchangeRate(this.usedCurrency, currency) * currency.getSellPrice();
                budget.set(budget.get() + transactionValue);
                currency.sell();
                tradeItems.remove(currency);
                break;
            }
        }
    }

    private void performBuyCompanyAction() {
        Exchange exchange = simulationInstance.getRandomExchange();
        try {
            Company company = exchange.getRandomCompany();
            double companyBuyPrice = CurrencyRatesBase.calculateExchangeRate(this.usedCurrency, exchange.getCurrency()) * company.getBuyPrice();
            if (companyBuyPrice > budget.get()) {
                return;
            } else {
                if (company.getStockCount() == 0) {
                   return;
                }
                company.buy();
                budget.set(budget.get() - companyBuyPrice);
                tradeItems.add(company);
            }
        } catch (NoCompanyException e) {
            return;
        }
    }

    private void performSellCompanyAction() {
        Company company;
        for (TradeItem tradeItem : tradeItems) {
            if (tradeItem.getClass().getSimpleName().equals("Company")) {
                company = (Company) tradeItem;
                Exchange exchange = (Exchange) company.getPartnerTradeMarket();
                double transactionValue = CurrencyRatesBase.calculateExchangeRate(this.usedCurrency, exchange.getCurrency()) * company.getSellPrice();
                budget.set(budget.get() + transactionValue);
                company.sell();
                tradeItems.remove(company);
                break;
            }
        }
    }

    private void performBuyMaterialAction() {
        MaterialMarket materialMarket = simulationInstance.getRandomMaterialMarket();
        try {
            Material material = materialMarket.getRandomMaterial();
            double materialBuyPrice = CurrencyRatesBase.calculateExchangeRate(this.usedCurrency, material.getCurrency()) * material.getBuyPrice();
            if (materialBuyPrice > budget.get()) {
                return;
            } else {
                material.buy();
                budget.set(budget.get() - materialBuyPrice);
                tradeItems.add(material);
            }
        } catch (NoMaterialException e) {
            return;
        }
    }

    private void performSellMaterialAction() {
        Material material;
        for (TradeItem tradeItem : tradeItems) {
            if (tradeItem.getClass().getSimpleName().equals("Material")) {
                material = (Material) tradeItem;
                double transactionValue = CurrencyRatesBase.calculateExchangeRate(this.usedCurrency, material.getCurrency()) * material.getSellPrice();
                budget.set(budget.get() + transactionValue);
                material.sell();
                tradeItems.remove(material);
                break;
            }
        }
    }

    protected void goBankrupt() {
        this.th.interrupt();
        this.timer.cancel();
    }
}
