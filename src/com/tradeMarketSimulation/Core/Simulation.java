package com.tradeMarketSimulation.Core;

import com.github.javafaker.Faker;
import com.tradeMarketSimulation.Core.Exceptions.NoMoreCurrencyException;
import com.tradeMarketSimulation.Core.Exceptions.NoMoreTradeMarketException;
import com.tradeMarketSimulation.Core.agents.Fund;
import com.tradeMarketSimulation.Core.agents.Investor;
import com.tradeMarketSimulation.Core.company.Company;
import com.tradeMarketSimulation.Core.simulationUtlis.Country;
import com.tradeMarketSimulation.Core.simulationUtlis.MaterialUnit;
import com.tradeMarketSimulation.Core.stockIndexes.StockIndex;
import com.tradeMarketSimulation.Core.tradeItems.Currency;
import com.tradeMarketSimulation.Core.tradeItems.Material;
import com.tradeMarketSimulation.Core.tradeMarkets.CurrencyMarket;
import com.tradeMarketSimulation.Core.tradeMarkets.Exchange;
import com.tradeMarketSimulation.Core.tradeMarkets.MaterialMarket;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Simulation {
    public ArrayList<Country> countries = new ArrayList<>();
    public ArrayList<Exchange> exchanges = new ArrayList<>();
    public ArrayList<CurrencyMarket> currencyMarkets = new ArrayList<>();
    public ArrayList<MaterialMarket> materialMarkets = new ArrayList<>();
    public ArrayList<Currency> currencies = new ArrayList<>();
    public ArrayList<Material> materials = new ArrayList<>();
    public ArrayList<Investor> investors = new ArrayList<>();
    public ArrayList<Fund> funds = new ArrayList<>();
    public ArrayList<Company> companies = new ArrayList<>();
    public ArrayList<StockIndex> stockIndices = new ArrayList<>();
    public static ObjectProperty<Date> simulationDate = new SimpleObjectProperty<>();

    public Simulation() {
        initStaticFields();
        runSeeders();
        progressSimulation();
    }

    private void runSeeders() {
        seedCountries();
        seedCurrencyMarkets();
        seedMaterialsMarket();
        seedCurrencies();
        seedMaterials();
        seedStockExchanges();
        seedCompanies();
        seedStockIndices();
        seedInvestors();
        seedFunds();
    }

    private void seedCountries() {
        for (int i = 0; i < 24; i++) {
            countries.add(new Country(Faker.instance().address().country()));
        }
    }

    private void seedCurrencies() {
        for (int i = 0; i < 12; i++) {
            Currency currency = new Currency(
                Faker.instance().pokemon().name() + "Coin",
                Math.random() * 10 + 1,
                currencyMarkets.get(i % 6)
            );
            currency.addCountry(countries.get(i));
            currency.addCountry(countries.get(i+12));
            countries.get(i).setCurrency(currency);
            countries.get(i+12).setCurrency(currency);
            currencyMarkets.get(i % 6).addNewCurrency(currency);
            currencies.add(currency);

        }
    }

    private void seedMaterials() {
        for (int i = 0; i < 12; i++) {
            Material material = new Material(
                    Faker.instance().beer().name(),
                    Math.random() * 10 + 1,
                    MaterialUnit.getMaterialUnitAtRandom(),
                    currencies.get((int) Math.floor(Math.random() * currencies.size())),
                    materialMarkets.get(i % 6)
            );
            materials.add(material);
            materialMarkets.get(i % 6).addNewMaterial(material);
        }
    }

    private void seedCurrencyMarkets() {
        for (int i = 0; i < 6; i++) {
            currencyMarkets.add(new CurrencyMarket(
                Faker.instance().lordOfTheRings().location(),
                Math.random()
            ));
        }
    }

    private void seedMaterialsMarket() {
        for (int i = 0; i < 6; i++) {
            materialMarkets.add(new MaterialMarket(
                    Faker.instance().witcher().location(),
                    Math.random()
            ));
        }
    }

    private void seedStockExchanges() {
        for (int i = 0; i < 12; i++) {
            exchanges.add(new Exchange(
                    Faker.instance().harryPotter().location(),
                    Math.random(),
                    currencies.get(i % 12).getCountries().get(i % 2),
                    Faker.instance().address().city(),
                    Faker.instance().address().streetAddress(),
                    currencies.get(i % 12)
            ));
        }
    }

    private void seedCompanies() {
        for (int i = 0; i < 36; i++) {
            Company company = new Company(
                Faker.instance().hipster().word() + " Ltd.",
                Math.random() * 10 + 1,
                exchanges.get(i % 12)
            );
            exchanges.get(i % 12).addNewCompany(company);
            Thread th = new Thread(company);
            th.setDaemon(true);
            th.start();
            company.setTh(th);
            companies.add(company);
        }
    }

    private void seedStockIndices() {
        for (int i = 0; i < 4; i++) {
            StockIndex stockIndex = new StockIndex(
                Faker.instance().rockBand().name() + " Index"
            );
            stockIndices.add(stockIndex);
            stockIndex.addNewCompany(companies.get(i));
            stockIndex.addNewCompany(companies.get(i + 4));
            stockIndex.addNewCompany(companies.get(i + 8));
            stockIndex.addNewCompany(companies.get(i + 12));
            stockIndex.addNewCompany(companies.get(i + 16));
            stockIndex.addNewCompany(companies.get(i + 20));
            stockIndex.addNewCompany(companies.get(i + 24));
            stockIndex.addNewCompany(companies.get(i + 28));
        }
    }

    private void seedInvestors() {
        for (int i = 0; i < 150; i++) {
            Investor investor = new Investor(
                this,
                Math.random() * 1000,
                currencies.get((int) (Math.random() * currencies.size())),
                Faker.instance().name().firstName(),
                Faker.instance().name().lastName(),
                String.valueOf(Faker.instance().number().digits(12))
            );
            Thread th = new Thread(investor);
            th.setDaemon(true);
            th.start();
            investor.setTh(th);
            investors.add(investor);
        }
    }

    private void seedFunds() {
        for (int i = 0; i < 50; i++) {
            Fund fund = new Fund(
                    this,
                    Math.random() * 1000,
                    currencies.get((int) (Math.random() * currencies.size())),
                    Faker.instance().commerce().productName() + " Fund",
                    Faker.instance().name().firstName(),
                    Faker.instance().name().lastName()
            );
            Thread th = new Thread(fund);
            th.setDaemon(true);
            th.start();
            fund.setTh(th);
            funds.add(fund);
        }
    }

    private void progressSimulation() {
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(Math.random() * 3 + 1), ev -> {
            moveInTime();
        }));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    private void moveInTime() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(simulationDate.getValue());
        cal.add(Calendar.HOUR_OF_DAY, 1);
        simulationDate.setValue(cal.getTime());
    }

    private void initStaticFields() {
        simulationDate.setValue(new GregorianCalendar(2018, Calendar.JANUARY, 1).getTime());
    }

    public void createNewExchange(Exchange exchange) {
        exchanges.add(exchange);
    }

    public void createNewCurrency(Currency currency) {
        currencies.add(currency);
        CurrencyMarket partnerTradeMarket = (CurrencyMarket) currency.getPartnerTradeMarket();
        partnerTradeMarket.addNewCurrency(currency);
    }

    public void createNewMaterial(Material material) {
        materials.add(material);
        MaterialMarket partnerTradeMarket = (MaterialMarket) material.getPartnerTradeMarket();
        partnerTradeMarket.addNewMaterial(material);
    }

    public void createNewMaterialMarket(MaterialMarket materialMarket) {
        materialMarkets.add(materialMarket);
    }

    public void createNewCurrencyMarket(CurrencyMarket currencyMarket) {
        currencyMarkets.add(currencyMarket);
    }

    public void createNewCompany(Company company) {
        companies.add(company);
        Exchange partnerTradeMarket = (Exchange) company.getPartnerTradeMarket();
        partnerTradeMarket.addNewCompany(company);
        Thread th = new Thread(company);
        th.setDaemon(true);
        th.start();
        company.setTh(th);
    }

    public void createNewIndex(StockIndex index) {
        stockIndices.add(index);
        index.addNewCompany(companies.get((int) (Math.random() * companies.size())));
        index.addNewCompany(companies.get((int) (Math.random() * companies.size())));
        index.addNewCompany(companies.get((int) (Math.random() * companies.size())));
        index.addNewCompany(companies.get((int) (Math.random() * companies.size())));
        index.addNewCompany(companies.get((int) (Math.random() * companies.size())));
        index.addNewCompany(companies.get((int) (Math.random() * companies.size())));
        index.addNewCompany(companies.get((int) (Math.random() * companies.size())));
    }

    public void createNewInvestor(Investor investor) {
        investors.add(investor);
        Thread th = new Thread(investor);
        th.setDaemon(true);
        th.start();
        investor.setTh(th);
    }

    public void createNewFund(Fund fund) {
        funds.add(fund);
        Thread th = new Thread(fund);
        th.setDaemon(true);
        th.start();
        fund.setTh(th);
    }

    public CurrencyMarket getRandomCurrencyMarket() {
        return currencyMarkets.get((int) (Math.random() * currencyMarkets.size()));
    }

    public MaterialMarket getRandomMaterialMarket() {
        return materialMarkets.get((int) (Math.random() * materialMarkets.size()));
    }

    public Exchange getRandomExchange() {
        return exchanges.get((int) (Math.random() * exchanges.size()));
    }

    public void deleteInvestor(Investor investor) {
        investor.goBankrupt();
    }

    public void deleteFund(Fund fund) {
        fund.goBankrupt();
    }

    public void deleteStockExchange(Exchange exchange) throws NoMoreTradeMarketException {
        if (exchanges.size() > 1) {
            this.exchanges.remove(exchange);
            for(Company company : exchange.getCompanies()) {
                company.swapExchange(exchanges.get((int) (Math.random() * exchanges.size())));
            }
        } else {
            throw new NoMoreTradeMarketException();
        }

    }

    public void deleteCurrencyMarket(CurrencyMarket currencyMarket) throws NoMoreTradeMarketException {
        if (currencyMarkets.size() > 1) {
            this.currencyMarkets.remove(currencyMarket);
            for(Currency currency : currencyMarket.getCurrencies()) {
                currency.swapCurrencyMarket(currencyMarkets.get((int) (Math.random() * currencyMarkets.size())));
            }
        } else {
            throw new NoMoreTradeMarketException();
        }
    }

    public void deleteMaterialMarket(MaterialMarket materialMarket) throws NoMoreTradeMarketException {
        if (exchanges.size() > 1) {
            this.materialMarkets.remove(materialMarket);
            for(Material material : materialMarket.getMaterials()) {
                material.swapMaterialMarket(materialMarkets.get((int) (Math.random() * materialMarkets.size())));
            }
        } else {
            throw new NoMoreTradeMarketException();
        }
    }

    public void deleteMaterial(Material material) {
        MaterialMarket materialMarket = (MaterialMarket) material.getPartnerTradeMarket();
        materialMarket.detachMaterial(material);
        materials.remove(material);
    }

    public void deleteCurrency(Currency currency) throws NoMoreCurrencyException {
        if (currencies.size() > 1) {
            currencies.remove(currency);
            for (Country country : currency.getCountries()) {
                Currency newCurrency = currencies.get((int) (Math.random() * currencies.size()));
                newCurrency.addCountry(country);
                country.setCurrency(newCurrency);
            }
            CurrencyMarket currencyMarket = (CurrencyMarket) currency.getPartnerTradeMarket();
            currencyMarket.detachCurrency(currency);
            for(Exchange exchange : exchanges) {
                if (exchange.getCurrency().equals(currency)) {
                    exchange.setCurrency(currencies.get((int) (Math.random() * currencies.size())));
                }
            }
            for(Investor investor : investors) {
                if (investor.getUsedCurrency().equals(currency)) {
                    investor.setUsedCurrency(currencies.get((int) (Math.random() * currencies.size())));
                }
            }
            for(Fund fund : funds) {
                if (fund.getUsedCurrency().equals(currency)) {
                    fund.setUsedCurrency(currencies.get((int) (Math.random() * currencies.size())));
                }
            }
            for(Material material : materials) {
                if (material.getCurrency().equals(currency)) {
                    material.setCurrency(currencies.get((int) (Math.random() * currencies.size())));
                }
            }
        } else {
            throw new NoMoreCurrencyException();
        }

    }

    public void deleteCompany(Company company) {
        Exchange exchange = (Exchange) company.getPartnerTradeMarket();
        exchange.detachCompany(company);
        companies.remove(company);
    }

    public void deleteStockIndex(StockIndex stockIndex) {
        this.stockIndices.remove(stockIndex);
    }

    private void saveState() {}

    private void loadState() {}
}
