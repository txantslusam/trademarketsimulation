package com.tradeMarketSimulation.Core.stockIndexes;

import com.tradeMarketSimulation.Core.company.Company;

import java.util.LinkedHashSet;

public class StockIndex {
    private String name;
    private LinkedHashSet<Company> companies = new LinkedHashSet<>();

    public String getName() {
        return name;
    }

    public LinkedHashSet<Company> getCompanies() {
        return companies;
    }

    public void addNewCompany(Company company) {
        this.companies.add(company);
    }

    public double getValue() {
        double value = 0;
        for(Company company : companies) {
            value = value + company.getActualValue();
        }
        return value;
    }

    public StockIndex(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
