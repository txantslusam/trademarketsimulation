# Trade Market Simulation
Mateusz Tomkowiak inf132333

Grupa I6 - środa 8:00 - 9:30

---

W programie działamy w jednym oknie z kartami dla poszczególnych rzeczy. 

Projekt zawiera 2 zależności: 
* [JavaFaker](https://github.com/DiUS/java-faker) - Do generowania danych losowych 
* [JFoenix](https://github.com/jfoenixadmin/JFoenix) - Framework dla JavaFX zawierająca elementy z Material Design


